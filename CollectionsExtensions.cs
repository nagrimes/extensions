﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Collections.Generic;

namespace RotaryHeart.Lib.Extensions
{
    public static class CollectionsExtensions
    {
        
        /// <summary>
        /// Shuffle this array using the Fisher-Yates method.
        /// </summary>
        public static void Shuffle<T>(this T[] array)
        {
            System.Random rng = new System.Random();

            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
        }
        
        /// <summary>
        /// Return a random item from this array.
        /// </summary>
        public static T RandomItem<T>(this T[] array)
        {
            if (array.Length == 0)
                throw new System.IndexOutOfRangeException("Cannot select a random item from an empty list");

            return array[UnityEngine.Random.Range(0, array.Length)];
        }
        
        /// <summary>
        /// Shuffle this list using the Fisher-Yates method.
        /// </summary>
        public static void Shuffle<T>(this IList<T> list)
        {
            System.Random rng = new System.Random();

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Return a random item from this list.
        /// </summary>
        public static T RandomItem<T>(this IList<T> list)
        {
            if (list.Count == 0)
                throw new System.IndexOutOfRangeException("Cannot select a random item from an empty list");

            return list[UnityEngine.Random.Range(0, list.Count)];
        }

        /// <summary>
        /// Removes a random item from this list, returning that item.
        /// </summary>
        public static T RemoveRandom<T>(this IList<T> list)
        {
            if (list.Count == 0)
                throw new System.IndexOutOfRangeException("Cannot remove a random item from an empty list");

            int index = UnityEngine.Random.Range(0, list.Count);
            T item = list[index];
            list.RemoveAt(index);
            return item;
        }

        /// <summary>
        /// Returns a random key from this dictionary
        /// </summary>
        public static TKey RandomKey<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            int size = dict.Count;

            if (size == 0)
                throw new System.IndexOutOfRangeException("Cannot select a random key from an empty dictionary");

            List<TKey> tempList = new List<TKey>(dict.Keys);

            System.Random rand = new System.Random();
            return tempList[rand.Next(tempList.Count)];
        }

        /// <summary>
        /// Returns a random value from this dictionary
        /// </summary>
        public static TValue RandomValue<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            int size = dict.Count;

            if (size == 0)
                throw new System.IndexOutOfRangeException("Cannot select a random value from an empty dictionary");

            List<TValue> tempList = new List<TValue>(dict.Values);

            System.Random rand = new System.Random();
            return tempList[rand.Next(tempList.Count)];
        }

    }
}