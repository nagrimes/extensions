﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class ReflectionExtensions
    {
        
        #region Synchronous

        /// <summary>
        /// Returns an array of all the classes that inherit from this Class
        /// </summary>
        /// <param name="includeAbstract">Include abstract classes</param>
        public static System.Type[] GetChildrenClasses(this System.Type baseType, bool includeAbstract = true)
        {
            if (baseType == null)
                return null;

            List<System.Type> children = new List<System.Type>();

            foreach (Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (System.Type type in assembly.GetTypes())
                {
                    if (!includeAbstract && type.IsAbstract)
                        continue;

                    if (type.IsSubclassOf(baseType))
                    {
                        children.Add(type);
                    }
                }
            }

            return children.ToArray();
        }

        /// <summary>
        /// Returns an array of all the parents, starting from the first parent, Classes of this Class
        /// </summary>
        public static System.Type[] GetParentsClasses(this System.Type childType)
        {
            if (childType == null)
                return null;

            List<System.Type> parents = new List<System.Type>();
            System.Type baseType = childType.BaseType;

            while (baseType != null)
            {
                parents.Insert(0, baseType);
                baseType = baseType.BaseType;
            }

            return parents.ToArray();
        }

        #endregion Synchronous

        #region Asynchronous
        
        /// <summary>
        /// Returns an array of all the classes that inherit from this Class
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeAbstract">Include abstract classes</param>
        public static AsyncResult<System.Type[]> GetChildrenClassesAsync(this System.Type baseType, MonoBehaviour mono, bool includeAbstract = true)
        {
            AsyncResult<System.Type[]> result = new AsyncResult<System.Type[]>();

            if (baseType == null)
                return result;

            mono.StartCoroutine(GetChildrenClassesAsync(result, baseType, includeAbstract));

            return result;
        }

        /// <summary>
        /// Returns an array of all the classes that inherit from this Class
        /// </summary>
        /// <param name="includeAbstract">Include abstract classes</param>
        public static AsyncResult<System.Type[]> GetChildrenClassesAwait(this System.Type baseType, bool includeAbstract = true)
        {
            AsyncResult<System.Type[]> result = new AsyncResult<System.Type[]>();

            if (baseType == null)
                return result;

            GetChildrenClassesAwait(result, baseType, includeAbstract);

            return result;
        }

        static IEnumerator GetChildrenClassesAsync(AsyncResult<System.Type[]> result, System.Type baseType, bool includeAbstract = true)
        {
            List<System.Type> children = new List<System.Type>();

            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            result.Count = assemblies.Length;

            foreach (Assembly assembly in assemblies)
            {
                System.Type[] types = assembly.GetTypes();
                result.Count += types.Length;

                foreach (System.Type type in types)
                {
                    if (!includeAbstract && type.IsAbstract)
                        continue;

                    if (type.IsSubclassOf(baseType))
                    {
                        children.Add(type);
                    }

                    yield return null;
                    result.Next();
                }

                result.Next();
            }

            result.Result = children.ToArray();
            result.Done();
        }

        static async void GetChildrenClassesAwait(AsyncResult<System.Type[]> result, System.Type baseType, bool includeAbstract = true)
        {
            List<System.Type> children = new List<System.Type>();

            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            result.Count = assemblies.Length;

            foreach (Assembly assembly in assemblies)
            {
                System.Type[] types = assembly.GetTypes();
                result.Count += types.Length;

                foreach (System.Type type in types)
                {
                    if (!includeAbstract && type.IsAbstract)
                        continue;

                    if (type.IsSubclassOf(baseType))
                    {
                        children.Add(type);
                    }

                    await Task.Yield();
                    result.Next();
                }

                result.Next();
            }

            result.Result = children.ToArray();
            result.Done();
        }

        /// <summary>
        /// Returns an array of all the parents, starting from the first parent, Classes of this Class
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        public static AsyncResult<System.Type[]> GetParentsClassesAsync(this System.Type childType, MonoBehaviour mono)
        {
            AsyncResult<System.Type[]> result = new AsyncResult<System.Type[]>();

            if (childType == null)
                return result;

            mono.StartCoroutine(GetParentsClassesAsync(result, childType));

            return result;
        }

        /// <summary>
        /// Returns an array of all the parents, starting from the first parent, Classes of this Class
        /// </summary>
        public static AsyncResult<System.Type[]> GetParentsClassesAwait(this System.Type childType)
        {
            AsyncResult<System.Type[]> result = new AsyncResult<System.Type[]>();

            if (childType == null)
                return result;

            GetParentsClassesAwait(result, childType);

            return result;
        }

        static IEnumerator GetParentsClassesAsync(AsyncResult<System.Type[]> result, System.Type childType)
        {
            List<System.Type> parents = new List<System.Type>();
            System.Type baseType = childType.BaseType;

            while (baseType != null)
            {
                parents.Insert(0, baseType);
                baseType = baseType.BaseType;
                yield return null;
            }

            result.Result = parents.ToArray();
            result.Done();
        }

        static async void GetParentsClassesAwait(AsyncResult<System.Type[]> result, System.Type childType)
        {
            List<System.Type> parents = new List<System.Type>();
            System.Type baseType = childType.BaseType;

            while (baseType != null)
            {
                parents.Insert(0, baseType);
                baseType = baseType.BaseType;
                await Task.Yield();
            }

            result.Result = parents.ToArray();
            result.Done();
        }

        #endregion Asynchronous

        /// <summary>
        /// Returns the property <paramref name="propName"/> value from this object reference
        /// </summary>
        /// <param name="propName">Property name, separated with a . if it's inside a class reference</param>
        public static T GetPropValue<T>(this object obj, string propName, object[] index, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
        {
            return (T)obj.GetPropValue(propName, index, flags);
        }

        /// <summary>
        /// Returns the property <paramref name="propName"/> value from this object reference
        /// </summary>
        /// <param name="propName">Property name, separated with a . if it's inside a class reference</param>
        public static object GetPropValue(this object obj, string propName, object[] index, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
        {
            if (string.IsNullOrEmpty(propName))
                throw new System.ArgumentNullException(nameof(propName));

            string[] nameParts = propName.Split('.');

            if (nameParts.Length == 1)
            {
                PropertyInfo info = obj.GetType().GetProperty(propName, flags);
                
                if (info == null) { return null; }

                return info.GetValue(obj, index);
            }

            foreach (string part in nameParts)
            {
                if (obj == null) { return null; }

                System.Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part, flags);

                if (info == null) { return null; }

                obj = info.GetValue(obj, index);
            }
            return obj;
        }

        /// <summary>
        /// Returns the field <paramref name="fieldName"/> value from this object reference
        /// </summary>
        /// <param name="fieldName">Field name, separated with a . if it's inside a class reference</param>
        public static T GetFieldValue<T>(this object obj, string fieldName, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
        {
            return (T)obj.GetFieldValue(fieldName, flags);
        }

        /// <summary>
        /// Returns the field <paramref name="fieldName"/> value from this object reference
        /// </summary>
        /// <param name="fieldName">Field name, separated with a . if it's inside a class reference</param>
        public static object GetFieldValue(this object obj, string fieldName, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
        {
            if (string.IsNullOrEmpty(fieldName))
                throw new System.ArgumentNullException(nameof(fieldName));

            string[] nameParts = fieldName.Split('.');

            if (nameParts.Length == 1)
            {
                FieldInfo info = obj.GetType().GetField(fieldName, flags);
                
                if (info == null) { return null; }

                return info.GetValue(obj);
            }

            foreach (string part in nameParts)
            {
                if (obj == null) { return null; }

                System.Type type = obj.GetType();
                FieldInfo info = type.GetField(part, flags);

                if (info == null) { return null; }

                obj = info.GetValue(obj);
            }

            return obj;
        }

        /// <summary>
        /// Sets the property <paramref name="propName"/> value from this object
        /// </summary>
        /// <param name="propName">Property name, separated with a . if it's inside a class</param>
        /// <param name="value">Value to set</param>
        public static bool SetPropValue(this object obj, string propName, object value, object[] index, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
        {
            if (string.IsNullOrEmpty(propName))
                throw new System.ArgumentNullException(nameof(propName));
            
            string[] nameParts = propName.Split('.');

            if (nameParts.Length == 1)
            {
                PropertyInfo info = obj.GetType().GetProperty(propName, flags);

                if (info == null) { return false; }
                
                info.SetValue(obj, value, index);
            }
            else
            {
                PropertyInfo info = null;
                foreach (string part in nameParts)
                {
                    if (obj == null)
                        break;

                    System.Type type = obj.GetType();
                    PropertyInfo tmpInfo = type.GetProperty(part, flags);

                    if (tmpInfo == null)
                        break;

                    info = tmpInfo;
                    obj = tmpInfo.GetValue(obj, index);
                }

                if (info != null)
                {
                    info.SetValue(obj, value, index);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Sets the field <paramref name="fieldName"/> value from this object
        /// </summary>
        /// <param name="fieldName">Field name, separated with a . if it's inside a class</param>
        /// <param name="value">Value to set</param>
        public static bool SetFieldValue(this object obj, string fieldName, object value, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
        {
            if (string.IsNullOrEmpty(fieldName))
                throw new System.ArgumentNullException(nameof(fieldName));

            string[] nameParts = fieldName.Split('.');

            if (nameParts.Length == 1)
            {
                FieldInfo info = obj.GetType().GetField(fieldName, flags);

                if (info == null) { return false; }

                info.SetValue(obj, value);
            }
            else
            {
                FieldInfo info = null;
                foreach (string part in nameParts)
                {
                    if (obj == null)
                        break;

                    System.Type type = obj.GetType();
                    FieldInfo tmpInfo = type.GetField(part, flags);

                    if (tmpInfo == null)
                        break;

                    info = tmpInfo;
                    obj = tmpInfo.GetValue(obj);
                }

                if (info != null)
                {
                    info.SetValue(obj, value);
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
        
    }
}