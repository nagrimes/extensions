// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

[assembly: InternalsVisibleTo("RotaryHeart.Lib")]
namespace RotaryHeart.Lib
{
    public class AsyncResult<T>
    {
        public enum AsyncOperationStatus
        {
            None,
            Succeeded,
            Failed,
        }
        
        public event System.Action<AsyncResult<T>> Completed;

        int m_index;

        public T Result { get; internal set; }
        public int Count { get; internal set; }

        public bool IsDone { get; private set; }
        public float PercentComplete { get; private set; }
        public AsyncOperationStatus Status { get; private set; }

        public AsyncResult()
        {
            m_index = 0;
            Status = AsyncOperationStatus.None;
        }

        internal void Next()
        {
            m_index++;
            PercentComplete = (float)m_index / (float)Count;
        }

        internal void Done()
        {
            IsDone = true;
            PercentComplete = 1;
            Status = AsyncOperationStatus.Succeeded;

            Completed?.Invoke(this);
        }

        internal void Failed()
        {
            IsDone = true;
            PercentComplete = 1;
            Status = AsyncOperationStatus.Failed;

            Completed?.Invoke(this);
        }
    }
}

namespace RotaryHeart.Lib.Extensions
{
    public enum Axis
    {
        XY,
        XZ,
        YX,
        YZ,
        ZX,
        ZY
    }

    public static class ExtensionMethods
    {
        /// <summary>
        /// Returns the bounds of a nav mesh. NOTE Cache this value since it's a heavy operation that creates a mesh when called
        /// </summary>
        public static Bounds GetNavMeshBounds()
        {
            Mesh mesh = new Mesh();
            NavMeshTriangulation data = NavMesh.CalculateTriangulation();
            mesh.vertices = data.vertices;
            mesh.triangles = data.indices;
            Bounds bounds = mesh.bounds;
            Object.DestroyImmediate(mesh);

            return bounds;
        }

        /// <summary>
        /// Returns true if this polygon contains <paramref name="point"/>; otherwise, false
        /// </summary>
        /// <param name="point">Point to check</param>
        public static bool PolyContainsPoint(this Vector2[] polyPoints, Vector2 point)
        {
            bool contains = false;

            int j = polyPoints.Length - 1;

            for (int i = 0; i < polyPoints.Length; j = i++)
            {
                if (((polyPoints[i].y <= point.y && point.y < polyPoints[j].y) || (polyPoints[j].y <= point.y && point.y < polyPoints[i].y)) &&
                   (point.x < (polyPoints[j].x - polyPoints[i].x) * (point.y - polyPoints[i].y) / (polyPoints[j].y - polyPoints[i].y) + polyPoints[i].x))
                    contains = !contains;
            }

            return contains;
        }

        /// <summary>
        /// Returns true if position is over an UI object; otherwise, false
        /// </summary>
        /// <param name="screenPosition">Screen position to check</param>
        public static bool IsPointerOverUIObject(Vector2 screenPosition)
        {
            return GetUIObjectUnderPointer(screenPosition) != null;
        }

        /// <summary>
        /// Returns the UI GameObject under the <paramref name="screenPosition"/> if found; otherwise, null 
        /// </summary>
        /// <param name="screenPosition">Screen position to check</param>
        public static GameObject GetUIObjectUnderPointer(Vector2 screenPosition)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current)
            {
                position = screenPosition
            };
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0 ? results[0].gameObject : null;
        }
        
        /// <summary>
        /// Returns true every time the frame count meets <paramref name="frequency"/> count
        /// </summary>
        /// <param name="frequency">Frequency limiter</param>
        public static bool RateLimiter(int frequency)
        {
            return Time.frameCount % frequency == 0;
        }
        
        /// <summary>
        /// Returns constant screen space size for the given position
        /// </summary>
        /// <param name="position">World position of the gizmo</param>
        /// <param name="sizeScale">Used for adjusting the gizmo scale</param>
        public static float GetGizmoConstantSize(Vector3 position, float sizeScale = 80f)
        {
            Camera current = Camera.current;
            position = Gizmos.matrix.MultiplyPoint(position);
 
            if (current)
            {
                Transform transform = current.transform;
                Vector3 position2 = transform.position;
                float z = Vector3.Dot(position - position2, transform.TransformDirection(new Vector3(0f, 0f, 1f)));
                Vector3 a = current.WorldToScreenPoint(position2 + transform.TransformDirection(new Vector3(0f, 0f, z)));
                Vector3 b = current.WorldToScreenPoint(position2 + transform.TransformDirection(new Vector3(1f, 0f, z)));
                float magnitude = (a - b).magnitude;
                return sizeScale / Mathf.Max(magnitude, 0.0001f);
            }
 
            return 20f;
        }
        
    }
}
