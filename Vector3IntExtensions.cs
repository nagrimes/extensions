﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class Vector3IntExtensions
    {
        
        #region Clamping
        
        /// <summary>
        /// Clamps the Vector3Int individual values
        /// </summary>
        /// <param name="xRange">Range for vector x</param>
        /// <param name="yRange">Range for vector y</param>
        /// <param name="zRange">Range for vector z</param>
        public static Vector3Int Clamp(this Vector3Int vector, Vector2Int xRange, Vector2Int yRange, Vector2Int zRange)
        {
            return vector.Clamp(xRange.x, xRange.y, yRange.x, yRange.y, zRange.x, zRange.y);
        }
        /// <summary>
        /// Clamps the Vector3Int individual values
        /// </summary>
        /// <param name="xMin">Minimum value for vector x</param>
        /// <param name="xMax">Maximum value for vector x</param>
        /// <param name="yMin">Minimum value for vector y</param>
        /// <param name="yMax">Maximum value for vector y</param>
        /// <param name="zMin">Minimum value for vector z</param>
        /// <param name="zMax">Maximum value for vector z</param>
        public static Vector3Int Clamp(this Vector3Int vector, int xMin, int xMax, int yMin, int yMax, int zMin, int zMax)
        {
            int x = Mathf.Clamp(vector.x, xMin, xMax);
            int y = Mathf.Clamp(vector.y, yMin, yMax);
            int z = Mathf.Clamp(vector.z, zMin, zMax);
            
            return new Vector3Int(x, y, z);
        }
        /// <summary>
        /// Clamps the Vector3Int x value
        /// </summary>
        /// <param name="range">Range for vector x</param>
        public static Vector3Int ClampX(this Vector3Int vector, Vector2Int range)
        {
            return vector.ClampX(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector3Int x value
        /// </summary>
        /// <param name="min">Minimum value for vector x</param>
        /// <param name="max">Maximum value for vector x</param>
        public static Vector3Int ClampX(this Vector3Int vector, int min, int max)
        {
            int x = Mathf.Clamp(vector.x, min, max);
            
            return vector.WithX(x);
        }
        /// <summary>
        /// Clamps the Vector3Int y value
        /// </summary>
        /// <param name="range">Range for vector y</param>
        public static Vector3Int ClampY(this Vector3Int vector, Vector2Int range)
        {
            return vector.ClampY(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector3Int y value
        /// </summary>
        /// <param name="min">Minimum value for vector y</param>
        /// <param name="max">Maximum value for vector y</param>
        public static Vector3Int ClampY(this Vector3Int vector, int min, int max)
        {
            int y = Mathf.Clamp(vector.y, min, max);
            
            return vector.WithY(y);
        }
        /// <summary>
        /// Clamps the Vector3Int z value
        /// </summary>
        /// <param name="range">Range for vector z</param>
        public static Vector3Int ClampZ(this Vector3Int vector, Vector2Int range)
        {
            return vector.ClampZ(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector3Int z value
        /// </summary>
        /// <param name="min">Minimum value for vector z</param>
        /// <param name="max">Maximum value for vector z</param>
        public static Vector3Int ClampZ(this Vector3Int vector, int min, int max)
        {
            int z = Mathf.Clamp(vector.z, min, max);
            
            return vector.WithZ(z);
        }
        
        #endregion Clamping
        
        /// <summary>
        /// Compares a Vector3Int with this Vector3Int with an allowed offset
        /// </summary>
        /// <param name="other">Other Vector3Int to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this Vector3Int vector, Vector3Int other, float allowedDifference = 0)
#else
        public static bool Compare(this Vector3Int vector, in Vector3Int other, float allowedDifference = 0)
#endif
        {
            return (vector - other).sqrMagnitude <= allowedDifference;
        }

        /// <summary>
        /// Returns the Vector3Int with X value of <paramref name="x"/>
        /// </summary>
        /// <param name="x">Value to use</param>
        public static Vector3Int WithX(this Vector3Int vector, int x) => new Vector3Int(x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with Y value of <paramref name="y"/>
        /// </summary>
        /// <param name="y">Value to use</param>
        public static Vector3Int WithY(this Vector3Int vector, int y) => new Vector3Int(vector.x, y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with Z value of <paramref name="z"/>
        /// </summary>
        /// <param name="z">Value to use</param>
        public static Vector3Int WithZ(this Vector3Int vector, int z) => new Vector3Int(vector.x, vector.y, z);
        /// <summary>
        /// Converts the Vector3Int to a Vector3
        /// </summary>
        public static Vector3 ToVector3(this Vector3Int vector) => new Vector3(vector.x, vector.y, vector.z);
        /// <summary>
        /// Returns the absolute value of the vector
        /// </summary>
        public static Vector3Int Abs(this Vector3Int vector) => new Vector3Int(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector3Int StepTo(this Vector3Int vector, int step) => new Vector3Int(vector.x.StepTo(step), vector.y.StepTo(step), vector.z.StepTo(step));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector3Int StepTo(this Vector3Int vector, Vector3Int step) => new Vector3Int(vector.x.StepTo(step.x), vector.y.StepTo(step.y), vector.z.StepTo(step.z));
        
        #region Mathematics operations
        
        /// <summary>
        /// Returns the Vector3Int with <paramref name="x"/> added to the X value
        /// </summary>
        /// <param name="x">Value to add to X</param>
        public static Vector3Int AddToX(this Vector3Int vector, int x) => new Vector3Int(vector.x + x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="y"/> added to the Y value
        /// </summary>
        /// <param name="y">Value to add to Y</param>
        public static Vector3Int AddToY(this Vector3Int vector, int y) => new Vector3Int(vector.x, vector.y + y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="z"/> added to the Z value
        /// </summary>
        /// <param name="z">Value to add to Z</param>
        public static Vector3Int AddToZ(this Vector3Int vector, int z) => new Vector3Int(vector.x, vector.y, vector.z + z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="x"/> subtracted to the X value
        /// </summary>
        /// <param name="x">Value to subtract to X</param>
        public static Vector3Int SubtractToX(this Vector3Int vector, int x) => new Vector3Int(vector.x - x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="y"/> subtracted to the Y value
        /// </summary>
        /// <param name="y">Value to subtract to Y</param>
        public static Vector3Int SubtractToY(this Vector3Int vector, int y) => new Vector3Int(vector.x, vector.y - y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="z"/> subtracted to the Z value
        /// </summary>
        /// <param name="z">Value to subtract to Z</param>
        public static Vector3Int SubtractToZ(this Vector3Int vector, int z) => new Vector3Int(vector.x, vector.y, vector.z - z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="x"/> multiplied to the X value
        /// </summary>
        /// <param name="x">Value to multiply to X</param>
        public static Vector3Int MultiplyToX(this Vector3Int vector, int x) => new Vector3Int(vector.x * x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="y"/> multiplied to the Y value
        /// </summary>
        /// <param name="y">Value to multiply to Y</param>
        public static Vector3Int MultiplyToY(this Vector3Int vector, int y) => new Vector3Int(vector.x, vector.y * y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="z"/> multiplied to the Z value
        /// </summary>
        /// <param name="z">Value to multiply to Z</param>
        public static Vector3Int MultiplyToZ(this Vector3Int vector, int z) => new Vector3Int(vector.x, vector.y, vector.z * z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="x"/> divided to the X value
        /// </summary>
        /// <param name="x">Value to divide to X</param>
        public static Vector3Int DivideToX(this Vector3Int vector, int x) => new Vector3Int(vector.x / x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="y"/> divided to the Y value
        /// </summary>
        /// <param name="y">Value to divide to Y</param>
        public static Vector3Int DivideToY(this Vector3Int vector, int y) => new Vector3Int(vector.x, vector.y / y, vector.z);
        /// <summary>
        /// Returns the Vector3Int with <paramref name="z"/> divided to the Z value
        /// </summary>
        /// <param name="z">Value to divide to Z</param>
        public static Vector3Int DivideToZ(this Vector3Int vector, int z) => new Vector3Int(vector.x, vector.y, vector.z / z);

        #endregion Mathematics operations
        
        #region Conversion
        
        public static Vector2Int xx(this Vector3Int vector) => new Vector2Int(vector.x, vector.x);
        public static Vector2Int xy(this Vector3Int vector) => new Vector2Int(vector.x, vector.y);
        public static Vector2Int xz(this Vector3Int vector) => new Vector2Int(vector.x, vector.z);
        public static Vector2Int yx(this Vector3Int vector) => new Vector2Int(vector.y, vector.x);
        public static Vector2Int yy(this Vector3Int vector) => new Vector2Int(vector.y, vector.y);
        public static Vector2Int yz(this Vector3Int vector) => new Vector2Int(vector.y, vector.z);
        public static Vector2Int zx(this Vector3Int vector) => new Vector2Int(vector.z, vector.x);
        public static Vector2Int zy(this Vector3Int vector) => new Vector2Int(vector.z, vector.y);
        public static Vector2Int zz(this Vector3Int vector) => new Vector2Int(vector.z, vector.z);
        public static Vector3Int xxx(this Vector3Int vector) => new Vector3Int(vector.x, vector.x, vector.x);
        public static Vector3Int xxy(this Vector3Int vector) => new Vector3Int(vector.x, vector.x, vector.y);
        public static Vector3Int xxz(this Vector3Int vector) => new Vector3Int(vector.x, vector.x, vector.z);
        public static Vector3Int xyx(this Vector3Int vector) => new Vector3Int(vector.x, vector.y, vector.x);
        public static Vector3Int xyy(this Vector3Int vector) => new Vector3Int(vector.x, vector.y, vector.y);
        public static Vector3Int xyz(this Vector3Int vector) => new Vector3Int(vector.x, vector.y, vector.z);
        public static Vector3Int xzx(this Vector3Int vector) => new Vector3Int(vector.x, vector.z, vector.x);
        public static Vector3Int xzy(this Vector3Int vector) => new Vector3Int(vector.x, vector.z, vector.y);
        public static Vector3Int xzz(this Vector3Int vector) => new Vector3Int(vector.x, vector.z, vector.z);
        public static Vector3Int yxx(this Vector3Int vector) => new Vector3Int(vector.y, vector.x, vector.x);
        public static Vector3Int yxy(this Vector3Int vector) => new Vector3Int(vector.y, vector.x, vector.y);
        public static Vector3Int yxz(this Vector3Int vector) => new Vector3Int(vector.y, vector.x, vector.z);
        public static Vector3Int yyx(this Vector3Int vector) => new Vector3Int(vector.y, vector.y, vector.x);
        public static Vector3Int yyy(this Vector3Int vector) => new Vector3Int(vector.y, vector.y, vector.y);
        public static Vector3Int yyz(this Vector3Int vector) => new Vector3Int(vector.y, vector.y, vector.z);
        public static Vector3Int yzx(this Vector3Int vector) => new Vector3Int(vector.y, vector.z, vector.x);
        public static Vector3Int yzy(this Vector3Int vector) => new Vector3Int(vector.y, vector.z, vector.y);
        public static Vector3Int yzz(this Vector3Int vector) => new Vector3Int(vector.y, vector.z, vector.z);
        public static Vector3Int zxx(this Vector3Int vector) => new Vector3Int(vector.z, vector.x, vector.x);
        public static Vector3Int zxy(this Vector3Int vector) => new Vector3Int(vector.z, vector.x, vector.y);
        public static Vector3Int zxz(this Vector3Int vector) => new Vector3Int(vector.z, vector.x, vector.z);
        public static Vector3Int zyx(this Vector3Int vector) => new Vector3Int(vector.z, vector.y, vector.x);
        public static Vector3Int zyy(this Vector3Int vector) => new Vector3Int(vector.z, vector.y, vector.y);
        public static Vector3Int zyz(this Vector3Int vector) => new Vector3Int(vector.z, vector.y, vector.z);
        public static Vector3Int zzx(this Vector3Int vector) => new Vector3Int(vector.z, vector.z, vector.x);
        public static Vector3Int zzy(this Vector3Int vector) => new Vector3Int(vector.z, vector.z, vector.y);
        public static Vector3Int zzz(this Vector3Int vector) => new Vector3Int(vector.z, vector.z, vector.z);
        
        #endregion
        
    }
}