# Unity Extensions
This repo contains [extension methods](http://en.wikipedia.org/wiki/Extension_method) for Unity objects (`GameObject`, `Vector3`, etc.). These add useful functionality that helps avoiding reimplementation on multiple scripts.

# Usage
The class is inside the `RotaryHeart.Lib.Extensions` namespace, so you must import it into any script that you will be using the extensions with the `using` keyword:

```sh
using RotaryHeart.Lib.Extensions;
```

If you want to use the `Mathematics` and the `Async` extensions too you will need to uncomment the defines inside the ExtensionMethod script (first lines). Keep in mind that this is using the new Unity Packages so if you don't have them on your project you will get errors from the script.

# Importing
Add the scripts to your Unity project manually or use Unity Package Manager git setup to import it. Add the following to your project packages.json `"rotaryheart.lib.extensions": "https://gitlab.com/RotaryHeart-UnityShare/extensions.git"`
