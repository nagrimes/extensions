﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
#if Burst
using Unity.Burst;
#endif
#if Mathematics
using Unity.Mathematics;
#endif

namespace RotaryHeart.Lib.Extensions
{
#if Burst
    [BurstCompile]
#endif
    public static class IntExtensions
    {
        
        /// <summary>
        /// Ensures the int value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static int StepTo(this int i, int step)
        {
            return i - (i % step);
        }

#if Mathematics
        
        #region int2
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int2 WithX(this int2 value, int x) => new int2(x, value.y);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int2 WithY(this int2 value, int y) => new int2(value.x, y);
        
        #endregion int2
        
        #region int3
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int3 WithX(this int3 value, int x) => new int3(x, value.y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int3 WithY(this int3 value, int y) => new int3(value.x, y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int3 WithZ(this int3 value, int z) => new int3(value.x, value.y, z);
        
        #endregion
        
        #region int4
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int4 WithX(this int4 value, int x) => new int4(x, value.y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int4 WithY(this int4 value, int y) => new int4(value.x, y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int4 WithZ(this int4 value, int z) => new int4(value.x, value.y, z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int4 WithW(this int4 value, int w) => new int4(value.x, value.y, value.z, w);
        
        #endregion int4
        
        #region uint2
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint2 WithX(this uint2 value, uint x) => new uint2(x, value.y);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint2 WithY(this uint2 value, uint y) => new uint2(value.x, y);
        
        #endregion uint2
        
        #region uint3
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint3 WithX(this uint3 value, uint x) => new uint3(x, value.y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint3 WithY(this uint3 value, uint y) => new uint3(value.x, y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint3 WithZ(this uint3 value, uint z) => new uint3(value.x, value.y, z);
        
        #endregion
        
        #region uint4
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint4 WithX(this uint4 value, uint x) => new uint4(x, value.y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint4 WithY(this uint4 value, uint y) => new uint4(value.x, y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint4 WithZ(this uint4 value, uint z) => new uint4(value.x, value.y, z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static uint4 WithW(this uint4 value, uint w) => new uint4(value.x, value.y, value.z, w);
        
        #endregion uint4
        
#endif
        
    }
}