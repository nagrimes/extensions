﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class RectExtensions
    {
        
        #region Clamping
        
        /// <summary>
        /// Clamps the Rect individual values
        /// </summary>
        /// <param name="xRange">Range for Rect x</param>
        /// <param name="yRange">Range for Rect y</param>
        /// <param name="widthRange">Range for Rect z</param>
        /// <param name="heightRange">Range for Rect w</param>
        public static Rect Clamp(this Rect rect, Vector2 xRange, Vector2 yRange, Vector2 widthRange, Vector2 heightRange)
        {
            float x = Mathf.Clamp(rect.x, xRange.x, xRange.y);
            float y = Mathf.Clamp(rect.y, yRange.x, yRange.y);
            float width = Mathf.Clamp(rect.width, widthRange.x, widthRange.y);
            float height = Mathf.Clamp(rect.height, heightRange.x, heightRange.y);
            
            return new Rect(x, y, width, height);
        }
        /// <summary>
        /// Clamps the Rect individual values
        /// </summary>
        /// <param name="xMin">Minimum value for Rect x</param>
        /// <param name="xMax">Maximum value for Rect x</param>
        /// <param name="yMin">Minimum value for Rect y</param>
        /// <param name="yMax">Maximum value for Rect y</param>
        /// <param name="widthMin">Minimum value for Rect width</param>
        /// <param name="widthMax">Maximum value for Rect width</param>
        /// <param name="heightMin">Minimum value for Rect height</param>
        /// <param name="heightMax">Maximum value for Rect height</param>
        public static Rect Clamp(this Rect rect, float xMin, float xMax, float yMin, float yMax, float widthMin, float widthMax, float heightMin, float heightMax)
        {
            float x = Mathf.Clamp(rect.x, xMin, xMax);
            float y = Mathf.Clamp(rect.y, yMin, yMax);
            float width = Mathf.Clamp(rect.width, widthMin, widthMax);
            float height = Mathf.Clamp(rect.height, heightMin, heightMax);
            
            return new Rect(x, y, width, height);
        }
        /// <summary>
        /// Clamps the Rect x value
        /// </summary>
        /// <param name="range">Range for Rect x</param>
        public static Rect ClampX(this Rect rect, Vector2 range)
        {
            float x = Mathf.Clamp(rect.x, range.x, range.y);
            
            return rect.WithX(x);
        }
        /// <summary>
        /// Clamps the Rect x value
        /// </summary>
        /// <param name="min">Minimum value for Rect x</param>
        /// <param name="max">Maximum value for Rect x</param>
        public static Rect ClampX(this Rect rect, float min, float max)
        {
            float x = Mathf.Clamp(rect.x, min, max);
            
            return rect.WithX(x);
        }
        /// <summary>
        /// Clamps the Rect y value
        /// </summary>
        /// <param name="range">Range for Rect y</param>
        public static Rect ClampY(this Rect rect, Vector2 range)
        {
            float y = Mathf.Clamp(rect.y, range.x, range.y);
            
            return rect.WithY(y);
        }
        /// <summary>
        /// Clamps the Rect y value
        /// </summary>
        /// <param name="min">Minimum value for Rect y</param>
        /// <param name="max">Maximum value for Rect y</param>
        public static Rect ClampY(this Rect rect, float min, float max)
        {
            float y = Mathf.Clamp(rect.y, min, max);
            
            return rect.WithY(y);
        }
        /// <summary>
        /// Clamps the Rect width value
        /// </summary>
        /// <param name="range">Range for Rect width</param>
        public static Rect ClampWidth(this Rect rect, Vector2 range)
        {
            float width = Mathf.Clamp(rect.width, range.x, range.y);
            
            return rect.WithWidth(width);
        }
        /// <summary>
        /// Clamps the Rect width value
        /// </summary>
        /// <param name="min">Minimum value for Rect width</param>
        /// <param name="max">Maximum value for Rect width</param>
        public static Rect ClampWidth(this Rect rect, float min, float max)
        {
            float width = Mathf.Clamp(rect.width, min, max);
            
            return rect.WithWidth(width);
        }
        /// <summary>
        /// Clamps the Rect height value
        /// </summary>
        /// <param name="range">Range for Rect height</param>
        public static Rect ClampHeight(this Rect rect, Vector2 range)
        {
            float height = Mathf.Clamp(rect.height, range.x, range.y);
            
            return rect.WithHeight(height);
        }
        /// <summary>
        /// Clamps the Rect height value
        /// </summary>
        /// <param name="min">Minimum value for Rect height</param>
        /// <param name="max">Maximum value for Rect height</param>
        public static Rect ClampHeight(this Rect rect, float min, float max)
        {
            float height = Mathf.Clamp(rect.height, min, max);
            
            return rect.WithHeight(height);
        }
        
        #endregion Clamping
        
        /// <summary>
        /// Returns the Rect with X value of <paramref name="x"/>
        /// </summary>
        /// <param name="x">Value to use</param>
        public static Rect WithX(this Rect rect, float x) => new Rect(x, rect.y, rect.width, rect.height);
        /// <summary>
        /// Returns the Rect with Y value of <paramref name="y"/>
        /// </summary>
        /// <param name="y">Value to use</param>
        public static Rect WithY(this Rect rect, float y) => new Rect(rect.x, y, rect.width, rect.height);
        /// <summary>
        /// Returns the Rect with width value of <paramref name="width"/>
        /// </summary>
        /// <param name="width">Value to use</param>
        public static Rect WithWidth(this Rect rect, float width) => new Rect(rect.x, rect.y, width, rect.height);
        /// <summary>
        /// Returns the Rect with height value of <paramref name="height"/>
        /// </summary>
        /// <param name="height">Value to use</param>
        public static Rect WithHeight(this Rect rect, float height) => new Rect(rect.x, rect.y, rect.width, height);
        
        #region Mathematics operations
        
        /// <summary>
        /// Returns the Rect with <paramref name="x"/> added to the X value
        /// </summary>
        /// <param name="x">Value to add to X</param>
        public static Rect AddToX(this Rect rect, float x) => new Rect(rect.x + x, rect.y, rect.width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="y"/> added to the Y value
        /// </summary>
        /// <param name="y">Value to add to Y</param>
        public static Rect AddToY(this Rect re, float y) => new Rect(re.x, re.y + y, re.width, re.height);
        /// <summary>
        /// Returns the Rect with <paramref name="width"/> added to the width value
        /// </summary>
        /// <param name="width">Value to add to width</param>
        public static Rect AddToWidth(this Rect rec, float width) => new Rect(rec.x, rec.y, rec.width + width, rec.height);
        /// <summary>
        /// Returns the Rect with <paramref name="height"/> added to the height value
        /// </summary>
        /// <param name="height">Value to add to height</param>
        public static Rect AddToHeight(this Rect rec, float height) => new Rect(rec.x, rec.y, rec.width, rec.height + height);
        /// <summary>
        /// Returns the Rect with <paramref name="x"/> subtracted to the X value
        /// </summary>
        /// <param name="x">Value to subtract to X</param>
        public static Rect SubtractToX(this Rect rec, float x) => new Rect(rec.x - x, rec.y, rec.width, rec.height);
        /// <summary>
        /// Returns the Rect with <paramref name="y"/> subtracted to the Y value
        /// </summary>
        /// <param name="y">Value to subtract to Y</param>
        public static Rect SubtractToY(this Rect rec, float y) => new Rect(rec.x, rec.y - y, rec.width, rec.height);
        /// <summary>
        /// Returns the Rect with <paramref name="width"/> subtracted to the width value
        /// </summary>
        /// <param name="width">Value to subtract to width</param>
        public static Rect SubtractToWidth(this Rect rect, float width) => new Rect(rect.x, rect.y, rect.width - width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="height"/> subtracted to the height value
        /// </summary>
        /// <param name="height">Value to subtract to height</param>
        public static Rect SubtractToHeight(this Rect rect, float height) => new Rect(rect.x, rect.y, rect.width, rect.height - height);
        /// <summary>
        /// Returns the Rect with <paramref name="x"/> multiplied to the X value
        /// </summary>
        /// <param name="x">Value to multiply to X</param>
        public static Rect MultiplyToX(this Rect rect, float x) => new Rect(rect.x * x, rect.y, rect.width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="y"/> multiplied to the Y value
        /// </summary>
        /// <param name="y">Value to multiply to Y</param>
        public static Rect MultiplyToY(this Rect rect, float y) => new Rect(rect.x, rect.y * y, rect.width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="width"/> multiplied to the width value
        /// </summary>
        /// <param name="width">Value to multiply to width</param>
        public static Rect MultiplyToWidth(this Rect rect, float width) => new Rect(rect.x, rect.y, rect.width * width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="height"/> multiplied to the height value
        /// </summary>
        /// <param name="height">Value to multiply to height</param>
        public static Rect MultiplyToHeight(this Rect rect, float height) => new Rect(rect.x, rect.y, rect.width, rect.height * height);
        /// <summary>
        /// Returns the Rect with <paramref name="x"/> divided to the X value
        /// </summary>
        /// <param name="x">Value to divide to X</param>
        public static Rect DivideToX(this Rect rect, float x) => new Rect(rect.x / x, rect.y, rect.width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="y"/> divided to the Y value
        /// </summary>
        /// <param name="y">Value to divide to Y</param>
        public static Rect DivideToY(this Rect rect, float y) => new Rect(rect.x, rect.y / y, rect.width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="width"/> divided to the width value
        /// </summary>
        /// <param name="width">Value to divide to width</param>
        public static Rect DivideToWidth(this Rect rect, float width) => new Rect(rect.x, rect.y, rect.width / width, rect.height);
        /// <summary>
        /// Returns the Rect with <paramref name="height"/> divided to the height value
        /// </summary>
        /// <param name="height">Value to divide to height</param>
        public static Rect DivideToHeight(this Rect rect, float height) => new Rect(rect.x, rect.y, rect.width, rect.height / height);

        #endregion Mathematics operations
        
    }
}