﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class GameObjectExtensions
    {

        #region Synchronous

        /// <summary>
        /// Set the given layer recursively to all the children of this GameObject
        /// </summary>
        /// <param name="layer">Layer to set</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="ignoreLayer">Objects with the this layer will be ignored (not changed)</param>
        public static void SetLayerRecursively(this GameObject gameObject, int layer, bool includeInactive = false, int ignoreLayer = -2)
        {
            if (!includeInactive && !gameObject.activeInHierarchy)
                return;

            Transform[] children = gameObject.GetComponentsInChildren<Transform>(includeInactive);

            if (ignoreLayer != 2)
            {
                foreach (Transform obj in children)
                {
                    if (obj.gameObject.layer == ignoreLayer)
                        continue;

                    obj.gameObject.layer = layer;
                }
            }
            else
            {
                foreach (Transform obj in children)
                {
                    obj.gameObject.layer = layer;
                }
            }
        }

        /// <summary>
        /// Set the given layer recursively to all the children of this GameObject
        /// </summary>
        /// <param name="layer">Layer to set</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="ignoreLayer">Objects with the this layer will be ignored (not changed)</param>
        public static void SetLayerRecursively(this GameObject gameObject, string layer, bool includeInactive = false, string ignoreLayer = null)
        {
            gameObject.SetLayerRecursively(LayerMask.NameToLayer(layer), includeInactive,
                string.IsNullOrEmpty(ignoreLayer) ? -2 : LayerMask.NameToLayer(ignoreLayer));
        }

        /// <summary>
        /// Enables or disables all the <typeparamref name="T"/> Component on this GameObject and all its children.
        /// </summary>
        /// <param name="enabled">Enabled state True/False</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static void SetComponentsEnabledRecursively<T>(this GameObject gameObject, bool enabled, bool includeInactive = false) where T : MonoBehaviour
        {
            if (!includeInactive && !gameObject.activeInHierarchy)
                return;

            T[] components = gameObject.GetComponentsInChildren<T>(includeInactive);

            foreach (T component in components)
            {
                component.enabled = enabled;
            }
        }

        #endregion Synchronous

        #region Asynchronous

        /// <summary>
        /// Set the given layer recursively to all the children of this GameObject
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="layer">Layer to set</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="ignoreLayer">Objects with the this layer will be ignored (not changed)</param>
        public static AsyncResult<bool> SetLayerRecursivelyAsync(this GameObject gameObject, MonoBehaviour mono, int layer, bool includeInactive = false,
            int ignoreLayer = -2)
        {
            AsyncResult<bool> result = new AsyncResult<bool>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Result = true;
                result.Done();
                return result;
            }

            mono.StartCoroutine(SetLayerRecursivelyAsync(gameObject, result, layer, includeInactive, ignoreLayer));

            return result;
        }

        /// <summary>
        /// Set the given layer recursively to all the children of this GameObject
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="layer">Layer to set</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="ignoreLayer">Objects with the this layer will be ignored (not changed)</param>
        public static AsyncResult<bool> SetLayerRecursivelyAsync(this GameObject gameObject, MonoBehaviour mono, string layer, bool includeInactive = false,
            string ignoreLayer = null)
        {
            return gameObject.SetLayerRecursivelyAsync(mono, LayerMask.NameToLayer(layer), includeInactive);
        }

        /// <summary>
        /// Set the given layer recursively to all the children of this GameObject
        /// </summary>
        /// <param name="layer">Layer to set</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="ignoreLayer">Objects with the this layer will be ignored (not changed)</param>
        public static AsyncResult<bool> SetLayerRecursivelyAwait(this GameObject gameObject, int layer, bool includeInactive = false, int ignoreLayer = -2)
        {
            AsyncResult<bool> result = new AsyncResult<bool>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Result = true;
                result.Done();
                return result;
            }

            SetLayerRecursivelyAwait(gameObject, result, layer, includeInactive, ignoreLayer);

            return result;
        }

        /// <summary>
        /// Set the given layer recursively to all the children of this GameObject
        /// </summary>
        /// <param name="layer">Layer to set</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="ignoreLayer">Objects with the this layer will be ignored (not changed)</param>
        public static AsyncResult<bool> SetLayerRecursivelyAwait(this GameObject gameObject, string layer, bool includeInactive = false,
            string ignoreLayer = null)
        {
            return gameObject.SetLayerRecursivelyAwait(LayerMask.NameToLayer(layer), includeInactive);
        }

        static IEnumerator SetLayerRecursivelyAsync(GameObject gameObject, AsyncResult<bool> result, int layer, bool includeInactive, int ignoreLayer)
        {
            Transform[] children = gameObject.GetComponentsInChildren<Transform>(includeInactive);
            result.Count = children.Length;

            if (ignoreLayer != -2)
            {
                foreach (Transform obj in children)
                {
                    if (obj.gameObject.layer == ignoreLayer)
                        continue;

                    obj.gameObject.layer = layer;
                    result.Next();
                    yield return null;
                }
            }
            else
            {
                foreach (Transform obj in children)
                {
                    obj.gameObject.layer = layer;
                    result.Next();
                    yield return null;
                }
            }

            result.Result = true;
            result.Done();
        }

        static async void SetLayerRecursivelyAwait(GameObject gameObject, AsyncResult<bool> result, int layer, bool includeInactive, int ignoreLayer)
        {
            Transform[] children = gameObject.GetComponentsInChildren<Transform>(includeInactive);
            result.Count = children.Length;

            if (ignoreLayer != -2)
            {
                foreach (Transform obj in children)
                {
                    if (obj.gameObject.layer == ignoreLayer)
                        continue;

                    obj.gameObject.layer = layer;
                    result.Next();
                    await Task.Yield();
                }
            }
            else
            {
                foreach (Transform obj in children)
                {
                    obj.gameObject.layer = layer;
                    result.Next();
                    await Task.Yield();
                }
            }

            result.Result = true;
            result.Done();
        }

        /// <summary>
        /// Enables or disables all the <typeparamref name="T"/> Component on this GameObject and all its children.
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="enabled">Enabled state True/False</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<T[]> SetComponentsEnabledRecursivelyAsync<T>(this GameObject gameObject, MonoBehaviour mono, bool enabled,
            bool includeInactive = false) where T : MonoBehaviour
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Result = null;
                result.Done();
                return result;
            }

            mono.StartCoroutine(SetComponentsEnabledRecursivelyAsync(gameObject, result, enabled, includeInactive));

            return result;
        }

        /// <summary>
        /// Enables or disables all the <typeparamref name="T"/> Component on this GameObject and all its children.
        /// </summary>
        /// <param name="enabled">Enabled state True/False</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<T[]> SetComponentsEnabledRecursivelyAwait<T>(this GameObject gameObject, bool enabled,
            bool includeInactive = false) where T : MonoBehaviour
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Result = null;
                result.Done();
                return result;
            }

            SetComponentsEnabledRecursivelyAwait(gameObject, result, enabled, includeInactive);

            return result;
        }

        static IEnumerator SetComponentsEnabledRecursivelyAsync<T>(GameObject gameObject, AsyncResult<T[]> result, bool enabled, bool includeInactive)
            where T : MonoBehaviour
        {
            T[] components = gameObject.GetComponentsInChildren<T>(includeInactive);
            result.Count = components.Length;

            foreach (T component in components)
            {
                component.enabled = enabled;
                result.Next();
                yield return null;
            }

            result.Result = components;
            result.Done();
        }

        static async void SetComponentsEnabledRecursivelyAwait<T>(GameObject gameObject, AsyncResult<T[]> result, bool enabled, bool includeInactive)
            where T : MonoBehaviour
        {
            T[] components = gameObject.GetComponentsInChildren<T>(includeInactive);
            result.Count = components.Length;

            foreach (T component in components)
            {
                component.enabled = enabled;
                result.Next();
                await Task.Yield();
            }

            result.Result = components;
            result.Done();
        }

        #endregion Asynchronous

    }
}