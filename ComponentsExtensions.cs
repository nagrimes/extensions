﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class ComponentsExtensions
    {

        #region Single

        #region GetFirstParent

        /// <summary>
        /// Returns the first parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        public static T GetFirstParent<T>(this GameObject gameObject) where T : Component
        {
            T component = gameObject.GetComponent<T>();
            Transform temp = gameObject.transform.parent;

            while (temp != null)
            {
                if (temp.TryGetComponent(out T tempComponent))
                {
                    component = tempComponent;
                    break;
                }

                temp = temp.transform.parent;
            }

            return component;
        }

        /// <summary>
        /// Returns the first parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        public static T GetFirstParent<T>(this Component component) where T : Component
        {
            return component.gameObject.GetFirstParent<T>();
        }

        /// <summary>
        /// Returns the first parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        public static Component GetFirstParent(this GameObject gameObject, System.Type type)
        {
            Component component = gameObject.GetComponent(type);
            Transform temp = gameObject.transform.parent;

            while (temp != null)
            {
                if (temp.transform.parent.TryGetComponent(type, out Component tempComponent))
                {
                    component = tempComponent;
                    break;
                }

                temp = temp.transform.parent;
            }

            return component;
        }

        /// <summary>
        /// Returns the first parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        public static Component GetFirstParent(this Component component, System.Type type)
        {
            return component.gameObject.GetFirstParent(type);
        }

        #endregion GetFirstParent

        #region GetLastParent

        /// <summary>
        /// Returns the last parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        public static T GetLastParent<T>(this GameObject gameObject) where T : Component
        {
            T component = gameObject.GetComponent<T>();
            Transform temp = gameObject.transform.parent;

            while (temp != null)
            {
                if (temp.TryGetComponent(out T tempComponent))
                {
                    component = tempComponent;
                }

                temp = temp.transform.parent;
            }

            return component;
        }

        /// <summary>
        /// Returns the last parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        public static T GetLastParent<T>(this Component component) where T : Component
        {
            return component.gameObject.GetLastParent<T>();
        }

        /// <summary>
        /// Returns the last parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        public static Component GetLastParent(this GameObject gameObject, System.Type type)
        {
            Component component = gameObject.GetComponent(type);
            Transform temp = gameObject.transform.parent;

            while (temp != null)
            {
                if (temp.TryGetComponent(type, out Component tempComponent))
                {
                    component = tempComponent;
                }

                temp = temp.transform.parent;
            }

            return component;
        }

        /// <summary>
        /// Returns the last parent of this GameObject that has the same <paramref name="T"/> Component. If nothing is found, returns itself
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        public static Component GetLastParent(this Component component, System.Type type)
        {
            return component.gameObject.GetLastParent(type);
        }

        #endregion GetLastParent

        #region GetOrAddComponent

        /// <summary>
        /// Gets a component attached to this GameObject. If one isn't found, a new one is attached and returned.
        /// </summary>
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            if (!gameObject.TryGetComponent(out T comp))
            {
                comp = gameObject.AddComponent<T>();
            }

            return comp;
        }

        /// <summary>
        /// Gets a component attached to this GameObject. If one isn't found, a new one is attached and returned.
        /// </summary>
        public static T GetOrAddComponent<T>(this Component component) where T : Component
        {
            return component.gameObject.GetOrAddComponent<T>();
        }

        /// <summary>
        /// Gets a component attached to this GameObject. If one isn't found, a new one is attached and returned.
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        public static Component GetOrAddComponent(this GameObject gameObject, System.Type type)
        {
            if (!gameObject.TryGetComponent(out Component comp))
            {
                comp = gameObject.AddComponent(type);
            }

            return comp;
        }

        /// <summary>
        /// Gets a component attached to this GameObject. If one isn't found, a new one is attached and returned.
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        public static Component GetOrAddComponent(this Component component, System.Type type)
        {
            return component.gameObject.GetOrAddComponent(type);
        }

        #endregion GetOrAddComponent

        #region TryGetComponent
        
        /// <summary>
        /// Returns true if the component was found on this GameObject; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponent<T>(this GameObject gameObject, out T componentReference) where T : Component
        {
            componentReference = gameObject.GetComponent<T>();

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if the component was found on this Component GameObject; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponent<T>(this Component component, out T componentReference) where T : Component
        {
            return component.gameObject.TryGetComponent(out componentReference);
        }

        /// <summary>
        /// Returns true if the component was found on this GameObject; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponent(this GameObject gameObject, System.Type type, out Component componentReference)
        {
            componentReference = gameObject.GetComponent(type);

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if the component was found on this Component GameObject; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponent(this Component component, System.Type type, out Component componentReference)
        {
            return component.gameObject.TryGetComponent(type, out componentReference);
        }

        #endregion TryGetComponent

        #region TryGetComponentInChildren

        /// <summary>
        /// Returns true if any <typeparamref name="T"/> Component was found on this GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the search?</param>
        public static bool TryGetComponentInChildren<T>(this GameObject gameObject, out T componentReference, bool includeInactive = false) where T : Component
        {
            componentReference = gameObject.GetComponentInChildren<T>(includeInactive);

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if any <typeparamref name="T"/> Component was found on this GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the search?</param>
        public static bool TryGetComponentInChildren<T>(this Component component, out T componentReference, bool includeInactive = false) where T : Component
        {
            return component.gameObject.TryGetComponentInChildren(out componentReference, includeInactive);
        }

        /// <summary>
        /// Returns true if any <paramref name="type"/> Component was found on this GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the search?</param>
        public static bool TryGetComponentInChildren(this GameObject gameObject, System.Type type, out Component componentReference, bool includeInactive = false)
        {
            componentReference = gameObject.GetComponentInChildren(type, includeInactive);

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if any <paramref name="type"/> Component was found on this GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the search?</param>
        public static bool TryGetComponentInChildren(this Component component, System.Type type, out Component componentReference, bool includeInactive = false)
        {
            return component.gameObject.TryGetComponentInChildren(type, out componentReference, includeInactive);
        }

        #endregion TryGetComponentInchildren
        
        #region TryGetComponentInParent
        
        /// <summary>
        /// Returns true if the component was found on any of this GameObject parents; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponentInParent<T>(this GameObject gameObject, out T componentReference)
            where T : Component
        {
            componentReference = gameObject.GetComponentInParent<T>();

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if the component was found on any of this Component GameObject parents; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponentInParent<T>(this Component component, out T componentReference)
            where T : Component
        {
            return component.gameObject.TryGetComponentInParent(out componentReference);
        }

        /// <summary>
        /// Returns true if the component was found on any of this GameObject parents; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponentInParent(this GameObject gameObject, System.Type type,
                                                      out Component componentReference)
        {
            componentReference = gameObject.GetComponentInParent(type);

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if the component was found on any of this Component GameObject parents; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains the component reference if it is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponentInParent(this Component component, System.Type type,
                                                      out Component componentReference)
        {
            return component.gameObject.TryGetComponentInParent(type, out componentReference);
        }
        
        #endregion TryGetComponentInParent

        #region GetComponentInChildrenIgnoring

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component on the children of this GameObject, ignoring children that contains a <typeparamref name="T1"/> Component, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T GetComponentInChildrenIgnoring<T, T1>(this GameObject gameObject, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            T value = null;

            if (includeGameObject)
            {
                if (gameObject.TryGetComponent(out value) && !gameObject.GetComponent<T1>())
                {
                    return value;
                }
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeSelf)
                    continue;

                if (trans.childCount > 0)
                    return trans.GetComponentInChildrenIgnoring<T, T1>(includeInactive, true);
            }

            return value;
        }

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component on the children of this GameObject, ignoring children that contains a <typeparamref name="T1"/> Component, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T GetComponentInChildrenIgnoring<T, T1>(this Component component, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            return component.gameObject.GetComponentInChildrenIgnoring<T, T1>(includeInactive, includeGameObject);
        }

        /// <summary>
        /// Returns the first <paramref name="type"/> Component on the children of this GameObject, ignoring children that contains a <paramref name="type1"/> Component, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component GetComponentInChildrenIgnoring(this GameObject gameObject, System.Type type, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            Component value = null;

            if (includeGameObject)
            {
                if (gameObject.TryGetComponent(type, out value) && !gameObject.GetComponent(type1))
                {
                    return value;
                }
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeSelf)
                    continue;

                if (trans.childCount > 0)
                    return trans.GetComponentInChildrenIgnoring(type, type1, includeInactive, true);
            }

            return value;
        }

        /// <summary>
        /// Returns the <paramref name="type"/> Components on the children of this GameObject. Ignoring children that contains a <paramref name="type1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component GetComponentInChildrenIgnoring(this Component component, System.Type type, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentInChildrenIgnoring(type, type1, includeInactive, includeGameObject);
        }

        #endregion GetComponentInChildrenIgnoring

        #region GetComponentInTransformChildren

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T GetComponentInTransformChildren<T>(this GameObject gameObject, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            T value = null;

            if (includeGameObject)
            {
                if (gameObject.TryGetComponent(out value))
                    return value;
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeSelf)
                    continue;

                if (trans.TryGetComponent(out value))
                {
                    break;
                }
            }

            return value;
        }

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component found on this GameObject children, without including any grand children, if found;, otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T GetComponentInTransformChildren<T>(this Component component, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            return component.gameObject.GetComponentInTransformChildren<T>(includeInactive, includeGameObject);
        }

        /// <summary>
        /// Returns the first <paramref name="type"/> Component found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component GetComponentInTransformChildren(this GameObject gameObject, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            Component value = null;

            if (includeGameObject)
            {
                if (gameObject.TryGetComponent(type, out value))
                    return value;
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeSelf)
                    continue;

                if (trans.TryGetComponent(type, out value))
                {
                    break;
                }
            }

            return value;
        }

        /// <summary>
        /// Returns the first <paramref name="type"/> Component found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component GetComponentInTransformChildren(this Component component, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentInTransformChildren(type, includeInactive, includeGameObject);
        }

        #endregion GetComponentsInTransformChildren

        #region FindInScenes

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component on all open scenes, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="searchOnDontDestroy">Should the search include the DontDestroyOnLoad scene</param>
        public static T FindInAllScenes<T>(bool includeInactive = false, bool searchOnDontDestroy = true) where T : Component
        {
            T component = null;
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            for (int i = 0; i < sceneCount; i++)
            {
                component = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).FindInScene<T>(includeInactive);

                if (component != null)
                    return component;
            }

            if (searchOnDontDestroy)
            {
                GameObject temp = new GameObject();
                Object.DontDestroyOnLoad(temp);
                UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
                Object.DestroyImmediate(temp);

                component = dontDestroyOnLoad.FindInScene<T>(includeInactive);
            }

            return component;
        }

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component on the scene if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static T FindInScene<T>(this UnityEngine.SceneManagement.Scene scene, bool includeInactive = false) where T : Component
        {
            T component = null;
            GameObject[] sceneRoots = scene.GetRootGameObjects();

            foreach (GameObject go in sceneRoots)
            {
                if (!includeInactive && !go.activeSelf)
                    continue;

                if (go.TryGetComponentInChildren(out component, includeInactive))
                {
                    break;
                }
            }

            return component;
        }

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component found on all open scenes if found; otherwise, null
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="searchOnDontDestroy">Should the search include the DontDestroyOnLoad scene</param>
        public static Component FindInAllScenes(System.Type type, bool includeInactive = false, bool searchOnDontDestroy = true)
        {
            Component component = null;
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            for (int i = 0; i < sceneCount; i++)
            {
                component = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).FindInScene(type, includeInactive);

                if (component != null)
                    return component;
            }

            if (searchOnDontDestroy)
            {
                GameObject temp = new GameObject();
                Object.DontDestroyOnLoad(temp);
                UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
                Object.DestroyImmediate(temp);

                component = dontDestroyOnLoad.FindInScene(type, includeInactive);
            }

            return component;
        }

        /// <summary>
        /// Returns the first <typeparamref name="T"/> Component found on all open scenes if found; otherwise, null
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static Component FindInScene(this UnityEngine.SceneManagement.Scene scene, System.Type type, bool includeInactive = false)
        {
            Component component = null;
            GameObject[] sceneRoots = scene.GetRootGameObjects();

            for (int i = 0; i < sceneRoots.Length; i++)
            {
                GameObject go = sceneRoots[i];

                if (!includeInactive && !go.activeSelf)
                    continue;

                if (go.TryGetComponentInChildren(type, out component, includeInactive))
                {
                    break;
                }
            }

            return component;
        }

        #endregion FindInScenes

        #endregion

        #region Array

        #region TryGetComponents

        /// <summary>
        /// Returns true if any <typeparamref name="T"/> Component was found on this GameObject; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponents<T>(this GameObject gameObject, out T[] componentReference) where T : Component
        {
            componentReference = gameObject.GetComponents<T>();

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if any <typeparamref name="T"/> Component was found on this Component GameObject; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponents<T>(this Component component, out T[] componentReference) where T : Component
        {
            return component.gameObject.TryGetComponents(out componentReference);
        }

        /// <summary>
        /// Returns true if any <paramref name="type"/> Component was found on this GameObject; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponents(this GameObject gameObject, System.Type type, out Component[] componentReference)
        {
            componentReference = gameObject.GetComponents(type);

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if any <paramref name="type"/> Component was found on this Component GameObject; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        public static bool TryGetComponents(this Component component, System.Type type, out Component[] componentReference)
        {
            return component.gameObject.TryGetComponents(type, out componentReference);
        }

        #endregion TryGetComponents

        #region GetComponentsInChildrenIgnoring

        #region Synchronous

        /// <summary>
        /// Returns an array of all the <typeparamref name="T"/> Components on the children of this GameObject. Ignoring children that contains a <typeparamref name="T1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T[] GetComponentsInChildrenIgnoring<T, T1>(this GameObject gameObject, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            List<T> componentsList;

            if (includeGameObject)
            {
                if (gameObject.GetComponents<T1>() == null)
                    componentsList = new List<T>(gameObject.GetComponents<T>());
                else
                    componentsList = new List<T>();
            }
            else
            {
                componentsList = new List<T>();
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents<T>());

                if (trans.childCount > 0)
                    componentsList.AddRange(trans.GetComponentsInChildrenIgnoring<T, T1>(includeInactive, true));
            }

            return componentsList.ToArray();
        }

        /// <summary>
        /// Returns an array of all the <typeparamref name="T"/> Components on the children of this GameObject. Ignoring children that contains a <typeparamref name="T1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T[] GetComponentsInChildrenIgnoring<T, T1>(this Component component, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            return component.gameObject.GetComponentsInChildrenIgnoring<T, T1>(includeInactive, includeGameObject);
        }

        /// <summary>
        /// Returns an array of all the <paramref name="type"/> Components on the children of this GameObject. Ignoring children that contains a <paramref name="type1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component[] GetComponentsInChildrenIgnoring(this GameObject gameObject, System.Type type, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            List<Component> componentsList;

            if (includeGameObject)
            {
                if (gameObject.GetComponents(type1) == null)
                    componentsList = new List<Component>(gameObject.GetComponents(type));
                else
                    componentsList = new List<Component>();
            }
            else
            {
                componentsList = new List<Component>();
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents(type));

                if (trans.childCount > 0)
                    componentsList.AddRange(trans.GetComponentsInChildrenIgnoring(type, type1, includeInactive, true));
            }

            return componentsList.ToArray();
        }

        /// <summary>
        /// Returns an array of all the <paramref name="type"/> Components on the children of this GameObject. Ignoring children that contains a <paramref name="type1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component[] GetComponentsInChildrenIgnoring(this Component component, System.Type type, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentsInChildrenIgnoring(type, type1, includeInactive, includeGameObject);
        }

        #endregion

        #region Asynchronous

        /// <summary>
        /// Returns an array of all the <typeparamref name="T"/> Components on the children of this GameObject. Ignoring children that contains a <typeparamref name="T1"/> Component
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInChildrenIgnoringAsync<T, T1>(this GameObject gameObject, MonoBehaviour mono, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            mono.StartCoroutine(GetComponentsInChildrenIgnoringAsync<T, T1>(gameObject, mono, result, includeInactive, includeGameObject));

            return result;
        }

        /// <summary>
        /// Returns an array of all the <typeparamref name="T"/> Components on the children of this GameObject. Ignoring children that contains a <typeparamref name="T1"/> Component
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInChildrenIgnoringAsync<T, T1>(this Component component, MonoBehaviour mono, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            return component.gameObject.GetComponentsInChildrenIgnoringAsync<T, T1>(mono, includeInactive, includeGameObject);
        }
        
        /// <summary>
        /// Returns an array of all the <typeparamref name="T"/> Components on the children of this GameObject. Ignoring children that contains a <typeparamref name="T1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInChildrenIgnoringAwait<T, T1>(this GameObject gameObject, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            GetComponentsInChildrenIgnoringAwait<T, T1 >(gameObject, result, includeInactive, includeGameObject);
            
            return result;
        }

        /// <summary>
        /// Returns an array of all the <typeparamref name="T"/> Components on the children of this GameObject. Ignoring children that contains a <typeparamref name="T1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInChildrenIgnoringAwait<T, T1>(this Component component, bool includeInactive = false, bool includeGameObject = true) where T : Component where T1 : Component
        {
            return component.gameObject.GetComponentsInChildrenIgnoringAwait<T, T1>(includeInactive, includeGameObject);
        }
        
        static IEnumerator GetComponentsInChildrenIgnoringAsync<T, T1>(GameObject gameObject, MonoBehaviour mono, AsyncResult<T[]> result, bool includeInactive = false, bool includeGameObject = true, bool finish = true)
        {
            List<T> components;

            if (result.Result != null)
                components = new List<T>(result.Result);
            else
                components = new List<T>();

            if (includeGameObject && gameObject.GetComponent<T1>() == null)
                components.AddRange(gameObject.GetComponents<T>());

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy || trans.GetComponent<T1>() != null)
                    continue;

                components.AddRange(trans.GetComponents<T>());

                result.Count += trans.childCount;
                result.Next();
                yield return null;

                if (trans.childCount > 0)
                    yield return mono.StartCoroutine(GetComponentsInChildrenIgnoringAsync<T, T1>(gameObject, mono, result, includeInactive, false, false));
            }

            if (finish)
            {
                result.Result = components.ToArray();
                result.Done();
            }
        }
        
        static async void GetComponentsInChildrenIgnoringAwait<T, T1>(GameObject gameObject, AsyncResult<T[]> result, bool includeInactive = false, bool includeGameObject = true, bool finish = true)
        {
            await _GetComponentsInChildrenIgnoringAwait<T, T1>(gameObject, result, includeInactive, includeGameObject);
        }

        static async Task _GetComponentsInChildrenIgnoringAwait<T, T1>(GameObject gameObject, AsyncResult<T[]> result,
            bool includeInactive = false, bool includeGameObject = true, bool finish = true)
        {
            List<T> components;

            if (result.Result != null)
                components = new List<T>(result.Result);
            else
                components = new List<T>();

            if (includeGameObject && gameObject.GetComponent<T1>() == null)
                components.AddRange(gameObject.GetComponents<T>());

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy || trans.GetComponent<T1>() != null)
                    continue;

                components.AddRange(trans.GetComponents<T>());

                result.Count += trans.childCount;
                result.Next();
                await Task.Yield();

                if (trans.childCount > 0)
                    await _GetComponentsInChildrenIgnoringAwait<T, T1>(gameObject, result, includeInactive, false, false);
            }

            if (finish)
            {
                result.Result = components.ToArray();
                result.Done();
            }
        }

        /// <summary>
        /// Returns an array of all the <paramref name="type"/> Components on the children of this GameObject. Ignoring children that contains a <paramref name="type1"/> Component
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInChildrenIgnoringAsync(this GameObject gameObject, System.Type type, MonoBehaviour mono, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            mono.StartCoroutine(GetComponentsInChildrenIgnoringAsync(gameObject, mono, type, type1, result, includeInactive, includeGameObject));

            return result;
        }

        /// <summary>
        /// Returns an array of all the <paramref name="type"/> Components on the children of this GameObject. Ignoring children that contains a <paramref name="type1"/> Component
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInChildrenIgnoringAsync(this Component component, MonoBehaviour mono, System.Type type, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentsInChildrenIgnoringAsync(type, mono, type1, includeInactive, includeGameObject);
        }

        /// <summary>
        /// Returns an array of all the <paramref name="type"/> Components on the children of this GameObject. Ignoring children that contains a <paramref name="type1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInChildrenIgnoringAwait(this GameObject gameObject, System.Type type, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            GetComponentsInChildrenIgnoringAwait(gameObject, type, type1, result, includeInactive, includeGameObject);

            return result;
        }

        /// <summary>
        /// Returns an array of all the <paramref name="type"/> Components on the children of this GameObject. Ignoring children that contains a <paramref name="type1"/> Component
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInChildrenIgnoringAwait(this Component component, System.Type type, System.Type type1, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentsInChildrenIgnoringAwait(type, type1, includeInactive, includeGameObject);
        }

        static IEnumerator GetComponentsInChildrenIgnoringAsync(GameObject gameObject, MonoBehaviour mono, System.Type type, System.Type type1, AsyncResult<Component[]> result, bool includeInactive = false, bool includeGameObject = true, bool finish = true)
        {
            List<Component> components;

            if (result.Result != null)
                components = new List<Component>(result.Result);
            else
                components = new List<Component>();

            if (includeGameObject && gameObject.GetComponents(type1) == null)
                components.AddRange(gameObject.GetComponents(type));

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy || trans.GetComponent(type1) != null)
                    continue;

                components.AddRange(trans.GetComponents(type));

                result.Count += trans.childCount;
                result.Next();
                yield return null;

                if (trans.childCount > 0)
                    yield return mono.StartCoroutine(GetComponentsInChildrenIgnoringAsync(gameObject, mono, type, type1, result, includeInactive, false, false));
            }

            if (finish)
            {
                result.Result = components.ToArray();
                result.Done();
            }
        }
        
        static async void GetComponentsInChildrenIgnoringAwait(GameObject gameObject, System.Type type, System.Type type1, AsyncResult<Component[]> result, bool includeInactive = false, bool includeGameObject = true, bool finish = true)
        {
            await _GetComponentsInChildrenIgnoringAwait(gameObject, type, type1, result, includeInactive, includeGameObject);
        }

        static async Task _GetComponentsInChildrenIgnoringAwait(GameObject gameObject, System.Type type,
            System.Type type1, AsyncResult<Component[]> result, bool includeInactive = false,
            bool includeGameObject = true, bool finish = true)
        {
            List<Component> components;

            if (result.Result != null)
                components = new List<Component>(result.Result);
            else
                components = new List<Component>();

            if (includeGameObject && gameObject.GetComponents(type1) == null)
                components.AddRange(gameObject.GetComponents(type));

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy || trans.GetComponent(type1) != null)
                    continue;

                components.AddRange(trans.GetComponents(type));

                result.Count += trans.childCount;
                result.Next();
                await Task.Yield();

                if (trans.childCount > 0)
                    await _GetComponentsInChildrenIgnoringAwait(gameObject, type, type1, result, includeInactive, false, false);
            }

            if (finish)
            {
                result.Result = components.ToArray();
                result.Done();
            }
        }

        #endregion Asynchronous

        #endregion GetComponentsInChildrenIgnoring

        #region TryGetComponentsInChildren

        /// <summary>
        /// Returns true if any <typeparamref name="T"/> Component was found on this GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        public static bool TryGetComponentsInChildren<T>(this GameObject gameObject, out T[] componentReference, bool includeInactive = false) where T : Component
        {
            componentReference = gameObject.GetComponentsInChildren<T>(includeInactive);

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if any <typeparamref name="T"/> Component was found on this Component GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        public static bool TryGetComponentsInChildren<T>(this Component component, out T[] componentReference, bool includeInactive = false) where T : Component
        {
            return component.gameObject.TryGetComponentsInChildren(out componentReference, includeInactive);
        }

        /// <summary>
        /// Returns true if any <paramref name="type"/> Component was found on this GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        public static bool TryGetComponentsInChildren(this GameObject gameObject, System.Type type, out Component[] componentReference, bool includeInactive = false)
        {
            componentReference = gameObject.GetComponentsInChildren(type, includeInactive);

            return componentReference != null;
        }

        /// <summary>
        /// Returns true if any <paramref name="type"/> Component was found on this Component GameObject or any children; otherwise, false
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="componentReference">When this method returns, contains an array of all the components reference if any is found; otherwise, null. This parameter is passed uninitialized.</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        public static bool TryGetComponentsInChildren(this Component component, System.Type type, out Component[] componentReference, bool includeInactive = false)
        {
            return component.gameObject.TryGetComponentsInChildren(type, out componentReference, includeInactive);
        }

        #endregion TryGetComponentsInChildren

        #region GetComponentsInTransformChildren

        #region Synchronous

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T[] GetComponentsInTransformChildren<T>(this GameObject gameObject, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            if (!includeInactive && !gameObject.activeInHierarchy)
                return null;

            List<T> componentsList;

            if (includeGameObject)
            {
                componentsList = new List<T>(gameObject.GetComponents<T>());
            }
            else
            {
                componentsList = new List<T>();
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents<T>());
            }

            return componentsList.ToArray();
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this Transform children, without including any grand children
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static T[] GetComponentsInTransformChildren<T>(this Component component, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            return component.gameObject.GetComponentsInTransformChildren<T>(includeInactive);
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component[] GetComponentsInTransformChildren(this GameObject gameObject, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            if (!includeInactive && !gameObject.activeInHierarchy)
                return null;

            List<Component> componentsList;

            if (includeGameObject)
            {
                componentsList = new List<Component>(gameObject.GetComponents(type));
            }
            else
            {
                componentsList = new List<Component>();
            }

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents(type));
            }

            return componentsList.ToArray();
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this Transform children, without including any grand children
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static Component[] GetComponentsInTransformChildren(this Component component, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentsInTransformChildren(type, includeInactive, includeGameObject);
        }

        #endregion Synchronous

        #region Asynchronous

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInTransformChildrenAsync<T>(this GameObject gameObject, MonoBehaviour mono, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            mono.StartCoroutine(GetComponentsInTransformChildrenAsync(gameObject, result, includeInactive, includeGameObject));

            return result;
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInTransformChildrenAsync<T>(this Component component, MonoBehaviour mono, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            return component.gameObject.GetComponentsInTransformChildrenAsync<T>(mono, includeInactive, includeGameObject);
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInTransformChildrenAwait<T>(this GameObject gameObject, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            GetComponentsInTransformChildrenAwait(gameObject, result, includeInactive, includeGameObject);

            return result;
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children, if found; otherwise, null
        /// </summary>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<T[]> GetComponentsInTransformChildrenAwait<T>(this Component component, bool includeInactive = false, bool includeGameObject = true) where T : Component
        {
            return component.gameObject.GetComponentsInTransformChildrenAwait<T>(includeInactive, includeGameObject);
        }
        
        static IEnumerator GetComponentsInTransformChildrenAsync<T>(GameObject gameObject, AsyncResult<T[]> result, bool includeInactive = false, bool includeGameObject = true)
        {
            List<T> componentsList;

            if (includeGameObject)
            {
                componentsList = new List<T>(gameObject.GetComponents<T>());
            }
            else
            {
                componentsList = new List<T>();
            }

            result.Count = 1;

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents<T>());

                yield return null;
            }

            result.Done();
            result.Result = componentsList.ToArray();
        }

        static async void GetComponentsInTransformChildrenAwait<T>(GameObject gameObject, AsyncResult<T[]> result, bool includeInactive = false, bool includeGameObject = true)
        {
            List<T> componentsList;

            if (includeGameObject)
            {
                componentsList = new List<T>(gameObject.GetComponents<T>());
            }
            else
            {
                componentsList = new List<T>();
            }

            result.Count = 1;

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents<T>());

                await Task.Yield();
            }

            result.Done();
            result.Result = componentsList.ToArray();
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInTransformChildrenAsync(this GameObject gameObject, MonoBehaviour mono, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            mono.StartCoroutine(GetComponentsInTransformChildrenAsync(gameObject, type, result, includeInactive, includeGameObject));

            return result;
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this Transform children, without including any grand children
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInTransformChildrenAsync(this Component component, MonoBehaviour mono, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentsInTransformChildrenAsync(mono, type, includeInactive, includeGameObject);
        }
        
        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this GameObject children, without including any grand children
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should Components on inactive GameObjects be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInTransformChildrenAwait(this GameObject gameObject, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            if (!includeInactive && !gameObject.activeInHierarchy)
            {
                result.Done();
                return result;
            }

            GetComponentsInTransformChildrenAwait(gameObject, type, result, includeInactive, includeGameObject);

            return result;
        }

        /// <summary>
        /// Returns an array of all the <paramref name="T"/> Components found on this Transform children, without including any grand children
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should Components on inactive Transforms be included in the found set?</param>
        /// <param name="includeGameObject">Should the component be searched on the GameObject too</param>
        public static AsyncResult<Component[]> GetComponentsInTransformChildrenAwait(this Component component, System.Type type, bool includeInactive = false, bool includeGameObject = true)
        {
            return component.gameObject.GetComponentsInTransformChildrenAwait(type, includeInactive, includeGameObject);
        }

        static IEnumerator GetComponentsInTransformChildrenAsync(GameObject gameObject, System.Type type, AsyncResult<Component[]> result, bool includeInactive = false, bool includeGameObject = true)
        {
            List<Component> componentsList;

            if (includeGameObject)
            {
                componentsList = new List<Component>(gameObject.GetComponents(type));
            }
            else
            {
                componentsList = new List<Component>();
            }

            result.Count = 1;

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents(type));

                yield return null;
            }

            result.Done();
            result.Result = componentsList.ToArray();
        }

        static async void GetComponentsInTransformChildrenAwait(GameObject gameObject, System.Type type, AsyncResult<Component[]> result, bool includeInactive = false, bool includeGameObject = true)
        {
            List<Component> componentsList;

            if (includeGameObject)
            {
                componentsList = new List<Component>(gameObject.GetComponents(type));
            }
            else
            {
                componentsList = new List<Component>();
            }

            result.Count = 1;

            foreach (Transform trans in gameObject.transform)
            {
                if (!includeInactive && !trans.gameObject.activeInHierarchy)
                    continue;

                componentsList.AddRange(trans.GetComponents(type));

                await Task.Yield();
            }

            result.Done();
            result.Result = componentsList.ToArray();
        }

        #endregion

        #endregion GetComponentsInTransformChildren

        #region FindInScenes

        #region Synchronous

        /// <summary>
        /// Returns an array of <typeparamref name="T"/> Components found on all open scenes
        /// </summary>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="searchOnDontDestroy">Should the search include the DontDestroyOnLoad scene</param>
        public static T[] FindAllInAllScenes<T>(bool includeInactive = false, bool searchOnDontDestroy = true) where T : Component
        {
            List<T> components = new List<T>();
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            for (int i = 0; i < sceneCount; i++)
            {
                components.AddRange(UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).FindAllInScene<T>(includeInactive));
            }

            if (searchOnDontDestroy)
            {
                GameObject temp = new GameObject();
                Object.DontDestroyOnLoad(temp);
                UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
                Object.DestroyImmediate(temp);

                components.AddRange(dontDestroyOnLoad.FindAllInScene<T>(includeInactive));
            }

            return components.ToArray();
        }

        /// <summary>
        /// Returns an array of <typeparamref name="T"/> Components found on the current scene
        /// </summary>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static T[] FindAllInScene<T>(this UnityEngine.SceneManagement.Scene scene, bool includeInactive = false) where T : Component
        {
            List<T> components = new List<T>();
            GameObject[] sceneRoots = scene.GetRootGameObjects();

            foreach (GameObject go in sceneRoots)
            {
                if (!includeInactive && !go.activeSelf)
                    continue;

                components.AddRange(go.GetComponentsInChildren<T>(includeInactive));
            }

            return components.ToArray();
        }

        /// <summary>
        /// Returns an array of <paramref name="type"/> Components found on all open scenes
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="searchOnDontDestroy">Should the search include the DontDestroyOnLoad scene</param>
        public static Component[] FindAllInAllScenes(System.Type type, bool includeInactive = false, bool searchOnDontDestroy = true)
        {
            List<Component> components = new List<Component>();
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            for (int i = 0; i < sceneCount; i++)
            {
                components.AddRange(UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).FindAllInScene(type, includeInactive));
            }

            if (searchOnDontDestroy)
            {
                GameObject temp = new GameObject();
                Object.DontDestroyOnLoad(temp);
                UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
                Object.DestroyImmediate(temp);

                components.AddRange(dontDestroyOnLoad.FindAllInScene(type, includeInactive));
            }

            return components.ToArray();
        }

        /// <summary>
        /// Returns an array of <paramref name="type"/> Components found on the current scene
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static Component[] FindAllInScene(this UnityEngine.SceneManagement.Scene scene, System.Type type, bool includeInactive = false)
        {
            List<Component> components = new List<Component>();
            GameObject[] sceneRoots = scene.GetRootGameObjects();

            foreach (GameObject go in sceneRoots)
            {
                if (!includeInactive && !go.activeSelf)
                    continue;

                components.AddRange(go.GetComponentsInChildren(type, includeInactive));
            }

            return components.ToArray();
        }

        #endregion Synchronous

        #region Asynchronous

        /// <summary>
        /// Returns an array of <typeparamref name="T"/> Components found on all open scenes
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="searchOnDontDestroy">Should the search include the DontDestroyOnLoad scene</param>
        public static AsyncResult<T[]> FindAllInAllScenesAsync<T>(MonoBehaviour mono, bool includeInactive = false, bool searchOnDontDestroy = true) where T : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            mono.StartCoroutine(FindAllInAllScenesAsync(mono, result, includeInactive, searchOnDontDestroy));

            return result;
        }

        /// <summary>
        /// Returns an array of <typeparamref name="T"/> Components found on the current scene
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<T[]> FindAllInSceneAsync<T>(this UnityEngine.SceneManagement.Scene scene, MonoBehaviour mono, bool includeInactive = false) where T : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            mono.StartCoroutine(FindAllInSceneAsync(scene, result, true, includeInactive));

            return result;
        }

        /// <summary>
        /// Returns an array of <typeparamref name="T"/> Components found on all open scenes
        /// </summary>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        /// <param name="searchOnDontDestroy">Should the search include the DontDestroyOnLoad scene</param>
        public static AsyncResult<T[]> FindAllInAllScenesAwait<T>(bool includeInactive = false, bool searchOnDontDestroy = true) where T : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            FindAllInAllScenesAwait(result, includeInactive, searchOnDontDestroy);

            return result;
        }

        /// <summary>
        /// Returns an array of <typeparamref name="T"/> Components found on the current scene
        /// </summary>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<T[]> FindAllInSceneAwait<T>(this UnityEngine.SceneManagement.Scene scene, bool includeInactive = false) where T : Component
        {
            AsyncResult<T[]> result = new AsyncResult<T[]>();

            FindAllInSceneAwait(scene, result, true, includeInactive);

            return result;
        }
        
        static IEnumerator FindAllInAllScenesAsync<T>(MonoBehaviour mono, AsyncResult<T[]> result, bool includeInactive = false, bool searchOnDontDestroy = true) where T : Component
        {
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            result.Count = sceneCount + 1;

            for (int i = 0; i < sceneCount; i++)
            {
                UnityEngine.SceneManagement.Scene scene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i);

                yield return mono.StartCoroutine(FindAllInSceneAsync(scene, result, false, includeInactive));

                result.Next();
            }

            if (searchOnDontDestroy)
            {
                GameObject temp = new GameObject();
                Object.DontDestroyOnLoad(temp);
                UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
                Object.DestroyImmediate(temp);

                yield return mono.StartCoroutine(FindAllInSceneAsync(dontDestroyOnLoad, result, false, includeInactive));
            }

            result.Done();
        }

        static IEnumerator FindAllInSceneAsync<T>(UnityEngine.SceneManagement.Scene scene, AsyncResult<T[]> result, bool markDone, bool includeInactive = false) where T : Component
        {
            List<T> components;

            if (result.Result != null)
                components = new List<T>(result.Result);
            else
                components = new List<T>();

            GameObject[] sceneRoots = scene.GetRootGameObjects();

            result.Count += sceneRoots.Length;

            foreach (GameObject go in sceneRoots)
            {
                if (!includeInactive && !go.activeSelf)
                    continue;

                components.AddRange(go.GetComponentsInChildren<T>(includeInactive));
                result.Next();
                yield return null;
            }

            result.Result = components.ToArray();

            if (markDone)
                result.Done();
        }
        
        static async void FindAllInAllScenesAwait<T>(AsyncResult<T[]> result, bool includeInactive = false, bool searchOnDontDestroy = true) where T : Component
        {
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            result.Count = sceneCount + 1;

            for (int i = 0; i < sceneCount; i++)
            {
                UnityEngine.SceneManagement.Scene scene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i);

                 await _FindAllInSceneAwait(scene, result, false, includeInactive);

                result.Next();
            }

            if (searchOnDontDestroy)
            {
                GameObject temp = new GameObject();
                Object.DontDestroyOnLoad(temp);
                UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
                Object.DestroyImmediate(temp);

                await _FindAllInSceneAwait(dontDestroyOnLoad, result, false, includeInactive);
            }

            result.Done();
        }

        static async void FindAllInSceneAwait<T>(UnityEngine.SceneManagement.Scene scene, AsyncResult<T[]> result, bool markDone, bool includeInactive = false) where T : Component
        {
            await _FindAllInSceneAwait(scene, result, markDone, includeInactive);
        }

        static async Task _FindAllInSceneAwait<T>(UnityEngine.SceneManagement.Scene scene, AsyncResult<T[]> result, bool markDone, bool includeInactive = false) where T : Component
        {
            List<T> components;

            if (result.Result != null)
                components = new List<T>(result.Result);
            else
                components = new List<T>();

            GameObject[] sceneRoots = scene.GetRootGameObjects();

            result.Count += sceneRoots.Length;

            foreach (GameObject go in sceneRoots)
            {
                if (!includeInactive && !go.activeSelf)
                    continue;

                components.AddRange(go.GetComponentsInChildren<T>(includeInactive));
                result.Next();
                await Task.Yield();
            }

            result.Result = components.ToArray();

            if (markDone)
                result.Done();
        }

        /// <summary>
        /// Returns an array of <paramref name="type"/> Components found on all open scenes
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<Component[]> FindAllInAllScenesAsync(MonoBehaviour mono, System.Type type, bool includeInactive = false)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            mono.StartCoroutine(FindAllInAllSceneAsync(mono, result, type, includeInactive));

            return result;
        }

        /// <summary>
        /// Returns an array of <paramref name="type"/> Components found on the current scene
        /// </summary>
        /// <param name="mono">MonoBehaviour reference needed to start the coroutines</param>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<Component[]> FindAllInSceneAsync(this UnityEngine.SceneManagement.Scene scene, MonoBehaviour mono, System.Type type, bool includeInactive = false)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            mono.StartCoroutine(FindAllInSceneAsync(scene, result, type, true, includeInactive));

            return result;
        }
        
        /// <summary>
        /// Returns an array of <paramref name="type"/> Components found on all open scenes
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<Component[]> FindAllInAllScenesAwait(System.Type type, bool includeInactive = false)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            FindAllInAllSceneAwait(result, type, includeInactive);

            return result;
        }

        /// <summary>
        /// Returns an array of <paramref name="type"/> Components found on the current scene
        /// </summary>
        /// <param name="type">The type of the Component to retrieve</param>
        /// <param name="includeInactive">Should inactive GameObjects be included</param>
        public static AsyncResult<Component[]> FindAllInSceneAwait(this UnityEngine.SceneManagement.Scene scene, System.Type type, bool includeInactive = false)
        {
            AsyncResult<Component[]> result = new AsyncResult<Component[]>();

            FindAllInSceneAwait(scene, result, type, true, includeInactive);

            return result;
        }

        static IEnumerator FindAllInAllSceneAsync(MonoBehaviour mono, AsyncResult<Component[]> result, System.Type type, bool includeInactive = false)
        {
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            result.Count = sceneCount + 1;

            for (int i = 0; i < sceneCount; i++)
            {
                var scene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i);

                yield return mono.StartCoroutine(FindAllInSceneAsync(scene, result, type, false, includeInactive));

                result.Next();
            }

            result.Done();
        }

        static IEnumerator FindAllInSceneAsync(this UnityEngine.SceneManagement.Scene scene, AsyncResult<Component[]> result, System.Type type, bool markDone, bool includeInactive = false)
        {
            List<Component> components;

            if (result.Result != null)
                components = new List<Component>(result.Result);
            else
                components = new List<Component>();

            GameObject[] sceneRoots = scene.GetRootGameObjects();

            result.Count += sceneRoots.Length;

            foreach (GameObject go in sceneRoots)
            {
                if (!includeInactive && !go.activeSelf)
                    continue;

                components.AddRange(go.GetComponentsInChildren(type, includeInactive));
                result.Next();
                yield return null;
            }

            result.Result = components.ToArray();

            if (markDone)
                result.Done();
        }

        static async void FindAllInAllSceneAwait(AsyncResult<Component[]> result, System.Type type, bool includeInactive = false)
        {
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCount;

            result.Count = sceneCount + 1;

            for (int i = 0; i < sceneCount; i++)
            {
                var scene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(i);

                 await _FindAllInSceneAwait(scene, result, type, false, includeInactive);

                result.Next();
            }

            result.Done();
        }

        static async void FindAllInSceneAwait(this UnityEngine.SceneManagement.Scene scene, AsyncResult<Component[]> result, System.Type type, bool markDone, bool includeInactive = false)
        {
            await _FindAllInSceneAwait(scene, result, type, markDone, includeInactive);
        }

        static async Task _FindAllInSceneAwait(this UnityEngine.SceneManagement.Scene scene, AsyncResult<Component[]> result, System.Type type, bool markDone, bool includeInactive = false)
        {
            List<Component> components;

            if (result.Result != null)
                components = new List<Component>(result.Result);
            else
                components = new List<Component>();

            GameObject[] sceneRoots = scene.GetRootGameObjects();

            result.Count += sceneRoots.Length;

            foreach (GameObject go in sceneRoots)
            {
                if (!includeInactive && !go.activeSelf)
                    continue;

                components.AddRange(go.GetComponentsInChildren(type, includeInactive));
                result.Next();
                await Task.Yield();
            }

            result.Result = components.ToArray();

            if (markDone)
                result.Done();
        }

        #endregion Asynchronous

        #endregion FindInScenes

        #endregion

    }
}