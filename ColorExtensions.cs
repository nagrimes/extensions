﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class ColorExtensions
    {

        /// <summary>
        /// Gets the hue-saturation-brightness (HSB) brightness value for this color.
        /// </summary>
        public static float GetBrightness(this Color color)
        {
            float num = color.r;
            float num2 = color.g;
            float num3 = color.b;
            float num4 = num;
            float num5 = num;

            if (num2 > num4)
            {
                num4 = num2;
            }
            if (num3 > num4)
            {
                num4 = num3;
            }
            if (num2 < num5)
            {
                num5 = num2;
            }
            if (num3 < num5)
            {
                num5 = num3;
            }
            return (num4 + num5) / 2f;
        }

        /// <summary>
        /// Gets the hue-saturation-brightness (HSB) hue value for this color.
        /// </summary>
        public static float GetHue(this Color color)
        {
            if (color.r.Compare(color.g) && color.g.Compare(color.b))
                return 0f;

            float num = color.r;
            float num2 = color.g;
            float num3 = color.b;
            float num7 = 0;
            float num4 = num;
            float num5 = num;
            if (num2 > num4)
            {
                num4 = num2;
            }
            if (num3 > num4)
            {
                num4 = num3;
            }
            if (num2 < num5)
            {
                num5 = num2;
            }
            if (num3 < num5)
            {
                num5 = num3;
            }
            
            float num6 = num4 - num5;
            
            if (num.Compare(num4))
            {
                num7 = (num2 - num3) / num6;
            }
            else if (num2.Compare(num4))
            {
                num7 = 2f + ((num3 - num) / num6);
            }
            else if (num3.Compare(num4))
            {
                num7 = 4f + ((num - num2) / num6);
            }
            
            num7 *= 60f;
            
            if (num7 < 0f)
                num7 += 360f;
            
            return num7;
        }

        /// <summary>
        /// Gets the hue-saturation-brightness (HSB) saturation value for this color.
        /// </summary>
        public static float GetSaturation(this Color color)
        {
            float num = color.r;
            float num2 = color.g;
            float num3 = color.b;
            
            float num7 = 0f;
            float num4 = num;
            float num5 = num;
            
            if (num2 > num4)
            {
                num4 = num2;
            }
            if (num3 > num4)
            {
                num4 = num3;
            }
            if (num2 < num5)
            {
                num5 = num2;
            }
            if (num3 < num5)
            {
                num5 = num3;
            }
            
            if (num4.Compare(num5))
                return num7;
            
            float num6 = (num4 + num5) / 2f;
            
            if (num6 <= 0.5)
                return ((num4 - num5) / (num4 + num5));
            
            return ((num4 - num5) / ((2f - num4) - num5));
        }

        /// <summary>
        /// Gets the hue-saturation-brightness (HSB) brightness value for this color.
        /// </summary>
        public static float GetBrightness(this Color32 color)
        {
            return GetBrightness(new Color(color.r / 255f, color.g / 255f, color.b / 255f, color.a / 255f));
        }

        /// <summary>
        /// Gets the hue-saturation-brightness (HSB) hue value for this color.
        /// </summary>
        public static float GetHue(this Color32 color)
        {
            return GetHue(new Color(color.r / 255f, color.g / 255f, color.b / 255f, color.a / 255f));
        }

        /// <summary>
        /// Gets the hue-saturation-brightness (HSB) saturation value for this color.
        /// </summary>
        public static float GetSaturation(this Color32 color)
        {
            return GetSaturation(new Color(color.r / 255f, color.g / 255f, color.b / 255f, color.a / 255f));
        }

        /// <summary>
        /// Returns the Color with r value of <paramref name="r"/>
        /// </summary>
        /// <param name="r">Value to use</param>
        public static Color WithR(this Color color, float r)
        {
            return new Color(r, color.g, color.b, color.a);
        }
        /// <summary>
        /// Returns the Color with g value of <paramref name="g"/>
        /// </summary>
        /// <param name="g">Value to use</param>
        public static Color WithG(this Color color, float g)
        {
            return new Color(color.r, g, color.b, color.a);
        }
        /// <summary>
        /// Returns the Color with b value of <paramref name="b"/>
        /// </summary>
        /// <param name="b">Value to use</param>
        public static Color WithB(this Color color, float b)
        {
            return new Color(color.r, color.g, b, color.a);
        }
        /// <summary>
        /// Returns the Color with a value of <paramref name="a"/>
        /// </summary>
        /// <param name="a">Value to use</param>
        public static Color WithA(this Color color, float a)
        {
            return new Color(color.r, color.g, color.b, a);
        }
        /// <summary>
        /// Returns the Color with r value of <paramref name="r"/>
        /// </summary>
        /// <param name="r">Value to use</param>
        public static Color32 WithR(this Color32 color, byte r)
        {
            return new Color32(r, color.g, color.b, color.a);
        }
        /// <summary>
        /// Returns the Color with g value of <paramref name="g"/>
        /// </summary>
        /// <param name="g">Value to use</param>
        public static Color32 WithG(this Color32 color, byte g)
        {
            return new Color32(color.r, g, color.b, color.a);
        }
        /// <summary>
        /// Returns the Color with b value of <paramref name="b"/>
        /// </summary>
        /// <param name="b">Value to use</param>
        public static Color32 WithB(this Color32 color, byte b)
        {
            return new Color32(color.r, color.g, b, color.a);
        }
        /// <summary>
        /// Returns the Color with a value of <paramref name="a"/>
        /// </summary>
        /// <param name="a">Value to use</param>
        public static Color32 WithA(this Color32 color, byte a)
        {
            return new Color32(color.r, color.g, color.b, a);
        }

    }
}