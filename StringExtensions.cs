﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

namespace RotaryHeart.Lib.Extensions
{
    public static class StringExtensions
    {
        
        /// <summary>
        /// Returns a value indicating whether any of the specified substrings <paramref name="values"/> occurs within this string.
        /// </summary>
        /// <param name="values">The strings to seek</param>
        public static bool Contains(this string st, params string[] values)
        {
            foreach (string s in values)
            {
                if (st.Contains(s))
                    return true;
            }
            
            return false;
        }

    }
}