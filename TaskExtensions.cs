// ReSharper disable CheckNamespace

using System;
using System.Threading.Tasks;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class TaskExtensions
    {
        /// <summary>
        /// Executes the given Action <paramref name="action"/> when the task completes execution
        /// </summary>
        /// <param name="action">Action to invoke when the task completes</param>
        public static Task OnComplete(this Task task, Action action)
        {
            TaskCompletionSource<bool> srcTask = new TaskCompletionSource<bool>();
            task.ContinueWith(_ =>
            {
                try
                {
                    action?.Invoke();
                    srcTask.SetResult(true);
                }
                catch (Exception e)
                {
                    srcTask.SetException(e);
                    Debug.LogError(e);
                }
            }, TaskContinuationOptions.ExecuteSynchronously);

            return srcTask.Task;
        }
    
        /// <summary>
        /// Executes the given Action <paramref name="action"/> when the task completes execution
        /// </summary>
        /// <param name="action">Action to invoke when the task completes</param>
        public static Task<T> OnComplete<T>(this Task<T> task, Action<T> action)
        {
            TaskCompletionSource<T> srcTask = new TaskCompletionSource<T>();
            task.ContinueWith(_ =>
            {
                try
                {
                    action?.Invoke(task.Result);
                    srcTask.SetResult(task.Result);
                }
                catch (Exception e)
                {
                    srcTask.SetException(e);
                    Debug.LogError(e);
                }
            }, TaskContinuationOptions.ExecuteSynchronously);

            return srcTask.Task;
        }

        /// <summary>
        /// Attempts to get result of task within timeout, returns <paramref name="defaultValue"/> if it times out
        /// </summary>
        /// <param name="timeout">Timeout limit</param>
        /// <param name="defaultValue">Value to return if it fails</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException">Throws exception if timeout is negative</exception>
        public static async Task<T> Timeout<T>(this Task<T> task, TimeSpan timeout, T defaultValue = default)
        {
            if (timeout < TimeSpan.Zero)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout), $"Cannot be negative {timeout}");
            }
            if (timeout == TimeSpan.Zero)
            {
                if (task.IsCompleted && !task.IsFaulted && !task.IsCanceled)
                {
                    return task.Result;
                }
            }
            else
            {
                if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
                {
                    return task.Result;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Attempts to get result of task within timeout, returns <paramref name="timeoutMs"/> if it times out
        /// </summary>
        /// <param name="timeoutMs">Timeout limit in milliseconds</param>
        /// <param name="defaultValue">Value to return if it fails</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException">Throws exception if timeout is negative</exception>
        public static async Task<T> Timeout<T>(this Task<T> task, int timeoutMs, T defaultValue = default)
        {
            if (timeoutMs < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(timeoutMs), $"Cannot be negative {timeoutMs}");
            }
            if (timeoutMs == 0)
            {
                if (task.IsCompleted && !task.IsFaulted && !task.IsCanceled)
                {
                    return task.Result;
                }
            }
            else
            {
                if (await Task.WhenAny(task, Task.Delay(timeoutMs)) == task)
                {
                    return task.Result;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Allows to execute a task with fire-and-forget. It will log any exceptions that it throws
        /// </summary>
        /// <param name="logCancellation">Should task cancellation be logged</param>
        public static void DoNotAwait(this Task task, bool logCancellation = true)
        {
            task.ContinueWith(t =>
            {
                if (task.IsCanceled)
                {
                    if (logCancellation)
                    {
                        Debug.LogWarning("Task canceled");
                    }
                }
                else if (task.IsFaulted)
                {
                    Debug.LogError(task.Exception?.InnerException ?? task.Exception);
                }
            }, TaskContinuationOptions.ExecuteSynchronously);
        }

        /// <summary>
        /// Allows to execute a task with fire-and-forget. It will log any exceptions that it throws
        /// </summary>
        /// <param name="logCancellation">Should task cancellation be logged</param>
        public static void DoNotAwait<T>(this Task<T> task, bool logCancellation = true)
        {
            task.ContinueWith(t =>
            {
                if (task.IsCanceled)
                {
                    if (logCancellation)
                    {
                        Debug.LogWarning("Task canceled");
                    }
                }
                else if (task.IsFaulted)
                {
                    Debug.LogError(task.Exception?.InnerException ?? task.Exception);
                }
            }, TaskContinuationOptions.ExecuteSynchronously);
        }
    }
}