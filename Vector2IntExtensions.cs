﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class Vector2IntExtensions
    {
        
        #region Clamping
        
        /// <summary>
        /// Clamps the Vector2Int individual values
        /// </summary>
        /// <param name="xRange">Range for vector x</param>
        /// <param name="yRange">Range for vector y</param>
        public static Vector2Int Clamp(this Vector3Int vector, Vector2Int xRange, Vector2Int yRange)
        {
            return vector.Clamp(xRange.x, xRange.y, yRange.x, yRange.y);
        }
        /// <summary>
        /// Clamps the Vector2Int individual values
        /// </summary>
        /// <param name="xMin">Minimum value for vector x</param>
        /// <param name="xMax">Maximum value for vector x</param>
        /// <param name="yMin">Minimum value for vector y</param>
        /// <param name="yMax">Maximum value for vector y</param>
        public static Vector2Int Clamp(this Vector3Int vector, int xMin, int xMax, int yMin, int yMax)
        {
            int x = Mathf.Clamp(vector.x, xMin, xMax);
            int y = Mathf.Clamp(vector.y, yMin, yMax);
            
            return new Vector2Int(x, y);
        }
        /// <summary>
        /// Clamps the Vector2Int x value
        /// </summary>
        /// <param name="range">Range for vector x</param>
        public static Vector2Int ClampX(this Vector2Int vector, Vector2Int range)
        {
            return vector.ClampX(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector2Int x value
        /// </summary>
        /// <param name="min">Minimum value for vector x</param>
        /// <param name="max">Maximum value for vector x</param>
        public static Vector2Int ClampX(this Vector2Int vector, int min, int max)
        {
            int x = Mathf.Clamp(vector.x, min, max);
            
            return vector.WithX(x);
        }
        /// <summary>
        /// Clamps the Vector2Int y value
        /// </summary>
        /// <param name="range">Range for vector y</param>
        public static Vector2Int ClampY(this Vector2Int vector, Vector2Int range)
        {
            return vector.ClampY(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector2Int y value
        /// </summary>
        /// <param name="min">Minimum value for vector y</param>
        /// <param name="max">Maximum value for vector y</param>
        public static Vector2Int ClampY(this Vector2Int vector, int min, int max)
        {
            int y = Mathf.Clamp(vector.y, min, max);
            
            return vector.WithY(y);
        }
        
        #endregion Clamping

        /// <summary>
        /// Compares a Vector2Int with this Vector2Int with an allowed offset
        /// </summary>
        /// <param name="other">Other Vector2Int to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this Vector2Int vector, Vector2Int other, float allowedDifference = 0)
#else
        public static bool Compare(this Vector2Int vector, in Vector2Int other, float allowedDifference = 0)
#endif
        {
            return (vector - other).sqrMagnitude <= allowedDifference;
        }

        /// <summary>
        /// Returns the Vector2Int with X value of <paramref name="x"/>
        /// </summary>
        /// <param name="x">Value to use</param>
        public static Vector2Int WithX(this Vector2Int vector, int x) => new Vector2Int(x, vector.y);
        /// <summary>
        /// Returns the Vector2Int with Y value of <paramref name="y"/>
        /// </summary>
        /// <param name="y">Value to use</param>
        public static Vector2Int WithY(this Vector2Int vector, int y) => new Vector2Int(vector.x, y);
        /// <summary>
        /// Converts the Vector2Int to a Vector2
        /// </summary>
        public static Vector2 ToVector2(this Vector2Int vector) => new Vector2(vector.x, vector.y);
        /// <summary>
        /// Returns the absolute value of the vector
        /// </summary>
        public static Vector2Int Abs(this Vector2Int vector) => new Vector2Int(Mathf.Abs(vector.x), Mathf.Abs(vector.y));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector2Int StepTo(this Vector2Int vector, int step) => new Vector2Int(vector.x.StepTo(step), vector.y.StepTo(step));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector2Int StepTo(this Vector2Int vector, Vector2Int step) => new Vector2Int(vector.x.StepTo(step.x), vector.y.StepTo(step.y));

        #region Mathematics operations
        
        /// <summary>
        /// Returns the Vector2Int with <paramref name="x"/> added to the X value
        /// </summary>
        /// <param name="x">Value to add to X</param>
        public static Vector2Int AddToX(this Vector2Int vector, int x) => new Vector2Int(vector.x + x, vector.y);
        /// <summary>
        /// Returns the Vector2Int with <paramref name="y"/> added to the Y value
        /// </summary>
        /// <param name="y">Value to add to Y</param>
        public static Vector2Int AddToY(this Vector2Int vector, int y) => new Vector2Int(vector.x, vector.y + y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="x"/> subtracted to the X value
        /// </summary>
        /// <param name="x">Value to subtract to X</param>
        public static Vector2Int SubtractToX(this Vector2Int vector, int x) => new Vector2Int(vector.x - x, vector.y);
        /// <summary>
        /// Returns the Vector2Int with <paramref name="y"/> subtracted to the Y value
        /// </summary>
        /// <param name="y">Value to subtract to Y</param>
        public static Vector2Int SubtractToY(this Vector2Int vector, int y) => new Vector2Int(vector.x, vector.y - y);
        /// <summary>
        /// Returns the Vector2Int with <paramref name="x"/> multiplied to the X value
        /// </summary>
        /// <param name="x">Value to multiply to X</param>
        public static Vector2Int MultiplyToX(this Vector2Int vector, int x) => new Vector2Int(vector.x * x, vector.y);
        /// <summary>
        /// Returns the Vector2Int with <paramref name="y"/> multiplied to the Y value
        /// </summary>
        /// <param name="y">Value to multiply to Y</param>
        public static Vector2Int MultiplyToY(this Vector2Int vector, int y) => new Vector2Int(vector.x, vector.y * y);
        /// <summary>
        /// Returns the Vector2Int with <paramref name="x"/> divided to the X value
        /// </summary>
        /// <param name="x">Value to divide to X</param>
        public static Vector2Int DivideToX(this Vector2Int vector, int x) => new Vector2Int(vector.x / x, vector.y);
        /// <summary>
        /// Returns the Vector2Int with <paramref name="y"/> divided to the Y value
        /// </summary>
        /// <param name="y">Value to divide to Y</param>
        public static Vector2Int DivideToY(this Vector2Int vector, int y) => new Vector2Int(vector.x, vector.y / y);

        #endregion Mathematics operations
        
        #region Conversion
        
        public static Vector2Int xx(this Vector2Int vector) => new Vector2Int(vector.x, vector.x);
        public static Vector2Int xy(this Vector2Int vector) => new Vector2Int(vector.x, vector.y);
        public static Vector2Int yx(this Vector2Int vector) => new Vector2Int(vector.x, vector.x);
        public static Vector2Int yy(this Vector2Int vector) => new Vector2Int(vector.x, vector.y);
        public static Vector3Int xxx(this Vector2Int vector) => new Vector3Int(vector.x, vector.x, vector.x);
        public static Vector3Int xxy(this Vector2Int vector) => new Vector3Int(vector.x, vector.x, vector.y);
        public static Vector3Int xyx(this Vector2Int vector) => new Vector3Int(vector.x, vector.y, vector.x);
        public static Vector3Int xyy(this Vector2Int vector) => new Vector3Int(vector.x, vector.y, vector.y);
        public static Vector3Int yxx(this Vector2Int vector) => new Vector3Int(vector.y, vector.x, vector.x);
        public static Vector3Int yxy(this Vector2Int vector) => new Vector3Int(vector.y, vector.x, vector.y);
        public static Vector3Int yyx(this Vector2Int vector) => new Vector3Int(vector.y, vector.y, vector.x);
        public static Vector3Int yyy(this Vector2Int vector) => new Vector3Int(vector.y, vector.y, vector.y);
        
        #endregion Conversion
        
    }
}