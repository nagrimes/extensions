// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class CanvasExtensions
    {
        /// <summary>
        /// Returns the canvas position from a screen position
        /// </summary>
        /// <param name="screenPosition">Screen position to convert</param>
        public static Vector2 ScreenToCanvasPosition(this Canvas canvas, Vector2 screenPosition)
        {
            Vector2 viewportPosition = new Vector2(screenPosition.x / Screen.width, screenPosition.y / Screen.height);
            return ViewportToCanvasPosition(canvas, viewportPosition);
        }

        /// <summary>
        /// Returns the canvas position from camera viewport
        /// </summary>
        /// <param name="viewportPosition">Viewport position to convert</param>
        public static Vector2 ViewportToCanvasPosition(this Canvas canvas, Vector2 viewportPosition)
        {
            Vector2 centerBasedViewPortPosition = viewportPosition - new Vector2(.5f, .5f);
            RectTransform canvasRect = canvas.GetComponent<RectTransform>();
            Vector2 scale = canvasRect.sizeDelta;
            return Vector2.Scale(centerBasedViewPortPosition, scale);
        }
    }
}