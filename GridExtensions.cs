﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class GridExtensions
    {
        
        /// <summary>
        /// Converts a world position to a cell world position
        /// </summary>
        /// <param name="worldPos">World position to convert</param>
        public static Vector3 WorldToCellWorld(this Grid grid, Vector3 worldPos)
        {
            return grid.CellToWorld(grid.WorldToCell(worldPos));
        }

        /// <summary>
        /// Converts a world position to a cell local position
        /// </summary>
        /// <param name="worldPos">World position to convert</param>
        public static Vector3 WorldToCellLocal(this Grid grid, Vector3 worldPos)
        {
            return grid.CellToLocal(grid.WorldToCell(worldPos));
        }

        /// <summary>
        /// Converts a local position to a cell world position
        /// </summary>
        /// <param name="localPos">Local position to convert</param>
        public static Vector3 LocalToCellWorld(this Grid grid, Vector3 localPos)
        {
            return grid.CellToWorld(grid.LocalToCell(localPos));
        }

        /// <summary>
        /// Converts a world position to a cell center world position
        /// </summary>
        /// <param name="worldPos">World position to convert</param>
        public static Vector3 WorldToCellCenterWorld(this Grid grid, Vector3 worldPos)
        {
            return grid.GetCellCenterWorld(grid.WorldToCell(worldPos));
        }

        /// <summary>
        /// Converts a world position to a cell center local position
        /// </summary>
        /// <param name="worldPos">World position to convert</param>
        public static Vector3 WorldToCellCenterLocal(this Grid grid, Vector3 worldPos)
        {
            return grid.GetCellCenterLocal(grid.WorldToCell(worldPos));
        }

        /// <summary>
        /// Converts a local position to a cell center world position
        /// </summary>
        /// <param name="localPos">Local position to convert</param>
        public static Vector3 LocalToCellCenterWorld(this Grid grid, Vector3 localPos)
        {
            return grid.GetCellCenterWorld(grid.LocalToCell(localPos));
        }

    }
}