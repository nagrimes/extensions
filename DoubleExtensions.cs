﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
#if Mathematics
using Unity.Mathematics;
#endif

namespace RotaryHeart.Lib.Extensions
{
    public static class DoubleExtensions
    {
        
#if Mathematics

        #region double2
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double2 WithX(this double2 value, double x) => new double2(x, value.y);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double2 WithY(this double2 value, double y) => new double2(value.x, y);
        
        #endregion double2
        
        #region double3
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double3 WithX(this double3 value, double x) => new double3(x, value.y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double3 WithY(this double3 value, double y) => new double3(value.x, y, value.z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double3 WithZ(this double3 value, double z) => new double3(value.x, value.y, z);
        
        #endregion
        
        #region double4
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double4 WithX(this double4 value, double x) => new double4(x, value.y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double4 WithY(this double4 value, double y) => new double4(value.x, y, value.z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double4 WithZ(this double4 value, double z) => new double4(value.x, value.y, z, value.w);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double4 WithW(this double4 value, double w) => new double4(value.x, value.y, value.z, w);
        
        #endregion double4
#endif
        
    }
}