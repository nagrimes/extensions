﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
    public static class Vector2Extensions
    {
        
        #region Clamping
        
        /// <summary>
        /// Clamps the Vector2 individual values
        /// </summary>
        /// <param name="xRange">Range for vector x</param>
        /// <param name="yRange">Range for vector y</param>
        public static Vector2 Clamp(this Vector2 vector, Vector2 xRange, Vector2 yRange)
        {
            return vector.Clamp(xRange.x, xRange.y, yRange.x, yRange.y);
        }
        /// <summary>
        /// Clamps the Vector2 individual values
        /// </summary>
        /// <param name="xMin">Minimum value for vector x</param>
        /// <param name="xMax">Maximum value for vector x</param>
        /// <param name="yMin">Minimum value for vector y</param>
        /// <param name="yMax">Maximum value for vector y</param>
        public static Vector2 Clamp(this Vector2 vector, float xMin, float xMax, float yMin, float yMax)
        {
            float x = Mathf.Clamp(vector.x, xMin, xMax);
            float y = Mathf.Clamp(vector.y, yMin, yMax);
            
            return new Vector2(x, y);
        }
        /// <summary>
        /// Clamps the Vector2 x value
        /// </summary>
        /// <param name="range">Range for vector x</param>
        public static Vector2 ClampX(this Vector2 vector, Vector2 range)
        {
            return vector.ClampX(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector2 x value
        /// </summary>
        /// <param name="min">Minimum value for vector x</param>
        /// <param name="max">Maximum value for vector x</param>
        public static Vector2 ClampX(this Vector2 vector, float min, float max)
        {
            float x = Mathf.Clamp(vector.x, min, max);
            
            return vector.WithX(x);
        }
        /// <summary>
        /// Clamps the Vector2 y value
        /// </summary>
        /// <param name="range">Range for vector y</param>
        public static Vector2 ClampY(this Vector2 vector, Vector2 range)
        {
            return vector.ClampY(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector2 y value
        /// </summary>
        /// <param name="min">Minimum value for vector y</param>
        /// <param name="max">Maximum value for vector y</param>
        public static Vector2 ClampY(this Vector2 vector, float min, float max)
        {
            float y = Mathf.Clamp(vector.y, min, max);
            
            return vector.WithY(y);
        }
        
        #endregion Clamping
        
        /// <summary>
        /// Compares a Vector2 with this Vector2 with an allowed offset
        /// </summary>
        /// <param name="other">Other Vector3 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        public static bool Compare(this Vector2 vector, Vector2 other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this Vector2 vector, in Vector2 other, float allowedDifference = 0.01f)
#endif
        {
            return (vector - other).sqrMagnitude <= allowedDifference;
        }

        /// <summary>
        /// Returns the Vector2 with X value of <paramref name="x"/>
        /// </summary>
        /// <param name="x">Value to use</param>
        public static Vector2 WithX(this Vector2 vector, float x) => new Vector2(x, vector.y);
        /// <summary>
        /// Returns the Vector2 with Y value of <paramref name="y"/>
        /// </summary>
        /// <param name="y">Value to use</param>
        public static Vector2 WithY(this Vector2 vector, float y) => new Vector2(vector.x, y);
        /// <summary>
        /// Returns the absolute value of the vector
        /// </summary>
        public static Vector2 Abs(this Vector2 vector) => new Vector2(Mathf.Abs(vector.x), Mathf.Abs(vector.y));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector2 StepTo(this Vector2 vector, float step) => new Vector2(vector.x.StepTo(step), vector.y.StepTo(step));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector2 StepTo(this Vector2 vector, Vector2 step) => new Vector2(vector.x.StepTo(step.x), vector.y.StepTo(step.y));

        #region Mathematics operations
        
        /// <summary>
        /// Returns the Vector2 with <paramref name="x"/> added to the X value
        /// </summary>
        /// <param name="x">Value to add to X</param>
        public static Vector2 AddToX(this Vector2 vector, float x) => new Vector2(vector.x + x, vector.y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="y"/> added to the Y value
        /// </summary>
        /// <param name="y">Value to add to Y</param>
        public static Vector2 AddToY(this Vector2 vector, float y) => new Vector2(vector.x, vector.y + y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="x"/> subtracted to the X value
        /// </summary>
        /// <param name="x">Value to subtract to X</param>
        public static Vector2 SubtractToX(this Vector2 vector, float x) => new Vector2(vector.x - x, vector.y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="y"/> subtracted to the Y value
        /// </summary>
        /// <param name="y">Value to subtract to Y</param>
        public static Vector2 SubtractToY(this Vector2 vector, float y) => new Vector2(vector.x, vector.y - y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="x"/> multiplied to the X value
        /// </summary>
        /// <param name="x">Value to multiply to X</param>
        public static Vector2 MultiplyToX(this Vector2 vector, float x) => new Vector2(vector.x * x, vector.y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="y"/> multiplied to the Y value
        /// </summary>
        /// <param name="y">Value to multiply to Y</param>
        public static Vector2 MultiplyToY(this Vector2 vector, float y) => new Vector2(vector.x, vector.y * y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="x"/> divided to the X value
        /// </summary>
        /// <param name="x">Value to divide to X</param>
        public static Vector2 DivideToX(this Vector2 vector, float x) => new Vector2(vector.x / x, vector.y);
        /// <summary>
        /// Returns the Vector2 with <paramref name="y"/> divided to the Y value
        /// </summary>
        /// <param name="y">Value to divide to Y</param>
        public static Vector2 DivideToY(this Vector2 vector, float y) => new Vector2(vector.x, vector.y / y);

        #endregion Mathematics operations
        
        #region Conversions
        
        public static Vector2 xx(this Vector2 vector) => new Vector2(vector.x, vector.x);
        public static Vector2 xy(this Vector2 vector) => new Vector2(vector.x, vector.y);
        public static Vector2 yx(this Vector2 vector) => new Vector2(vector.y, vector.x);
        public static Vector2 yy(this Vector2 vector) => new Vector2(vector.y, vector.y);
        public static Vector3 xxx(this Vector2 vector) => new Vector3(vector.x, vector.x, vector.x);
        public static Vector3 xxy(this Vector2 vector) => new Vector3(vector.x, vector.x, vector.y);
        public static Vector3 xyx(this Vector2 vector) => new Vector3(vector.x, vector.y, vector.x);
        public static Vector3 xyy(this Vector2 vector) => new Vector3(vector.x, vector.y, vector.y);
        public static Vector3 yxx(this Vector2 vector) => new Vector3(vector.y, vector.x, vector.x);
        public static Vector3 yxy(this Vector2 vector) => new Vector3(vector.y, vector.x, vector.y);
        public static Vector3 yyx(this Vector2 vector) => new Vector3(vector.y, vector.y, vector.x);
        public static Vector3 yyy(this Vector2 vector) => new Vector3(vector.y, vector.y, vector.y);
        public static Vector4 xxxx(this Vector2 vector) => new Vector4(vector.x, vector.x, vector.x, vector.x);
        public static Vector4 xxxy(this Vector2 vector) => new Vector4(vector.x, vector.x, vector.x, vector.y);
        public static Vector4 xxyx(this Vector2 vector) => new Vector4(vector.x, vector.x, vector.y, vector.x);
        public static Vector4 xxyy(this Vector2 vector) => new Vector4(vector.x, vector.x, vector.y, vector.y);
        public static Vector4 xyxx(this Vector2 vector) => new Vector4(vector.x, vector.y, vector.x, vector.x);
        public static Vector4 xyxy(this Vector2 vector) => new Vector4(vector.x, vector.y, vector.x, vector.y);
        public static Vector4 xyyx(this Vector2 vector) => new Vector4(vector.x, vector.y, vector.y, vector.x);
        public static Vector4 xyyy(this Vector2 vector) => new Vector4(vector.x, vector.y, vector.y, vector.y);
        public static Vector4 yxxx(this Vector2 vector) => new Vector4(vector.y, vector.x, vector.x, vector.x);
        public static Vector4 yxxy(this Vector2 vector) => new Vector4(vector.y, vector.x, vector.x, vector.y);
        public static Vector4 yxyx(this Vector2 vector) => new Vector4(vector.y, vector.x, vector.y, vector.x);
        public static Vector4 yyxx(this Vector2 vector) => new Vector4(vector.y, vector.y, vector.x, vector.x);
        public static Vector4 yyyx(this Vector2 vector) => new Vector4(vector.y, vector.y, vector.y, vector.x);
        public static Vector4 yyyy(this Vector2 vector) => new Vector4(vector.y, vector.y, vector.y, vector.y);
        
        #endregion Conversions
        
    }
}