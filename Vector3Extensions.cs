﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

using System.Runtime.CompilerServices;
#if Burst
using Unity.Burst;
#endif
using UnityEngine;

namespace RotaryHeart.Lib.Extensions
{
#if Burst
    [BurstCompile]
#endif
    public static class Vector3Extensions
    {
        
        #region Clamping
        
        /// <summary>
        /// Clamps the Vector3 individual values
        /// </summary>
        /// <param name="xRange">Range for vector x</param>
        /// <param name="yRange">Range for vector y</param>
        /// <param name="zRange">Range for vector z</param>
        public static Vector3 Clamp(this Vector3 vector, Vector2 xRange, Vector2 yRange, Vector2 zRange)
        {
            return vector.Clamp(xRange.x, xRange.y, yRange.x, yRange.y, zRange.x, zRange.y);
        }
        /// <summary>
        /// Clamps the Vector3 individual values
        /// </summary>
        /// <param name="xMin">Minimum value for vector x</param>
        /// <param name="xMax">Maximum value for vector x</param>
        /// <param name="yMin">Minimum value for vector y</param>
        /// <param name="yMax">Maximum value for vector y</param>
        /// <param name="zMin">Minimum value for vector z</param>
        /// <param name="zMax">Maximum value for vector z</param>
        public static Vector3 Clamp(this Vector3 vector, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax)
        {
            float x = Mathf.Clamp(vector.x, xMin, xMax);
            float y = Mathf.Clamp(vector.y, yMin, yMax);
            float z = Mathf.Clamp(vector.z, zMin, zMax);
            
            return new Vector3(x, y, z);
        }
        /// <summary>
        /// Clamps the Vector3 x value
        /// </summary>
        /// <param name="range">Range for vector x</param>
        public static Vector3 ClampX(this Vector3 vector, Vector2 range)
        {
            return vector.ClampX(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector3 x value
        /// </summary>
        /// <param name="min">Minimum value for vector x</param>
        /// <param name="max">Maximum value for vector x</param>
        public static Vector3 ClampX(this Vector3 vector, float min, float max)
        {
            float x = Mathf.Clamp(vector.x, min, max);
            
            return vector.WithX(x);
        }
        /// <summary>
        /// Clamps the Vector3 y value
        /// </summary>
        /// <param name="range">Range for vector y</param>
        public static Vector3 ClampY(this Vector3 vector, Vector2 range)
        {
            return vector.ClampY(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector3 y value
        /// </summary>
        /// <param name="min">Minimum value for vector y</param>
        /// <param name="max">Maximum value for vector y</param>
        public static Vector3 ClampY(this Vector3 vector, float min, float max)
        {
            float y = Mathf.Clamp(vector.y, min, max);
            
            return vector.WithY(y);
        }
        /// <summary>
        /// Clamps the Vector3 z value
        /// </summary>
        /// <param name="range">Range for vector z</param>
        public static Vector3 ClampZ(this Vector3 vector, Vector2 range)
        {
            return vector.ClampZ(range.x, range.y);
        }
        /// <summary>
        /// Clamps the Vector3 z value
        /// </summary>
        /// <param name="min">Minimum value for vector z</param>
        /// <param name="max">Maximum value for vector z</param>
        public static Vector3 ClampZ(this Vector3 vector, float min, float max)
        {
            float z = Mathf.Clamp(vector.z, min, max);
            
            return vector.WithZ(z);
        }
        
        #endregion Clamping
        
        /// <summary>
        /// Compares a Vector3 with this Vector3 with an allowed offset
        /// </summary>
        /// <param name="other">Other Vector3 to compare</param>
        /// <param name="allowedDifference">Allowed offset</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
#if Burst
        [BurstCompile]
        public static bool Compare(this Vector3 vector, Vector3 other, float allowedDifference = 0.01f)
#else
        public static bool Compare(this Vector3 vector, in Vector3 other, float allowedDifference = 0.01f)
#endif
        {
            return (vector - other).sqrMagnitude <= allowedDifference;
        }

        /// <summary>
        /// Returns the Vector3 with X value of <paramref name="x"/>
        /// </summary>
        /// <param name="x">Value to use</param>
        public static Vector3 WithX(this Vector3 vector, float x) => new Vector3(x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3 with Y value of <paramref name="y"/>
        /// </summary>
        /// <param name="y">Value to use</param>
        public static Vector3 WithY(this Vector3 vector, float y) => new Vector3(vector.x, y, vector.z);
        /// <summary>
        /// Returns the Vector3 with Z value of <paramref name="z"/>
        /// </summary>
        /// <param name="z">Value to use</param>
        public static Vector3 WithZ(this Vector3 vector, float z) => new Vector3(vector.x, vector.y, z);
        /// <summary>
        /// Returns the absolute value of the vector
        /// </summary>
        public static Vector3 Abs(this Vector3 vector) => new Vector3(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector3 StepTo(this Vector3 vector, float step) => new Vector3(vector.x.StepTo(step), vector.y.StepTo(step), vector.z.StepTo(step));
        /// <summary>
        /// Ensures the vector value is a multiple of <paramref name="step"/>
        /// </summary>
        /// <param name="step">Step to use</param>
        public static Vector3 StepTo(this Vector3 vector, Vector3 step) => new Vector3(vector.x.StepTo(step.x), vector.y.StepTo(step.y), vector.z.StepTo(step.z));

        #region Mathematics operations
        
        /// <summary>
        /// Returns the Vector3 with <paramref name="x"/> added to the X value
        /// </summary>
        /// <param name="x">Value to add to X</param>
        public static Vector3 AddToX(this Vector3 vector, float x) => new Vector3(vector.x + x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="y"/> added to the Y value
        /// </summary>
        /// <param name="y">Value to add to Y</param>
        public static Vector3 AddToY(this Vector3 vector, float y) => new Vector3(vector.x, vector.y + y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="z"/> added to the Z value
        /// </summary>
        /// <param name="z">Value to add to Z</param>
        public static Vector3 AddToZ(this Vector3 vector, float z) => new Vector3(vector.x, vector.y, vector.z + z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="x"/> subtracted to the X value
        /// </summary>
        /// <param name="x">Value to subtract to X</param>
        public static Vector3 SubtractToX(this Vector3 vector, float x) => new Vector3(vector.x - x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="y"/> subtracted to the Y value
        /// </summary>
        /// <param name="y">Value to subtract to Y</param>
        public static Vector3 SubtractToY(this Vector3 vector, float y) => new Vector3(vector.x, vector.y - y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="width"/> subtracted to the Z value
        /// </summary>
        /// <param name="width">Value to subtract to Z</param>
        public static Vector3 SubtractToZ(this Vector3 vector, float width) => new Vector3(vector.x, vector.y, vector.z - width);
        /// <summary>
        /// Returns the Vector3 with <paramref name="x"/> multiplied to the X value
        /// </summary>
        /// <param name="x">Value to multiply to X</param>
        public static Vector3 MultiplyToX(this Vector3 vector, float x) => new Vector3(vector.x * x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="y"/> multiplied to the Y value
        /// </summary>
        /// <param name="y">Value to multiply to Y</param>
        public static Vector3 MultiplyToY(this Vector3 vector, float y) => new Vector3(vector.x, vector.y * y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="width"/> multiplied to the Z value
        /// </summary>
        /// <param name="width">Value to multiply to Z</param>
        public static Vector3 MultiplyToZ(this Vector3 vector, float width) => new Vector3(vector.x, vector.y, vector.z * width);
        /// <summary>
        /// Returns the Vector3 with <paramref name="x"/> divided to the X value
        /// </summary>
        /// <param name="x">Value to divide to X</param>
        public static Vector3 DivideToX(this Vector3 vector, float x) => new Vector3(vector.x / x, vector.y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="y"/> divided to the Y value
        /// </summary>
        /// <param name="y">Value to divide to Y</param>
        public static Vector3 DivideToY(this Vector3 vector, float y) => new Vector3(vector.x, vector.y / y, vector.z);
        /// <summary>
        /// Returns the Vector3 with <paramref name="width"/> divided to the Z value
        /// </summary>
        /// <param name="width">Value to divide to Z</param>
        public static Vector3 DivideToZ(this Vector3 vector, float width) => new Vector3(vector.x, vector.y, vector.z / width);

        #endregion Mathematics operations
        
        #region Conversions

        public static Vector2 xx(this Vector3 vector) => new Vector2(vector.x, vector.x);
        public static Vector2 xy(this Vector3 vector) => new Vector2(vector.x, vector.y);
        public static Vector2 xz(this Vector3 vector) => new Vector2(vector.x, vector.z);
        public static Vector2 yx(this Vector3 vector) => new Vector2(vector.x, vector.x);
        public static Vector2 yy(this Vector3 vector) => new Vector2(vector.y, vector.y);
        public static Vector2 yz(this Vector3 vector) => new Vector2(vector.y, vector.z);
        public static Vector2 zx(this Vector3 vector) => new Vector2(vector.z, vector.x);
        public static Vector2 zy(this Vector3 vector) => new Vector2(vector.z, vector.y);
        public static Vector2 zz(this Vector3 vector) => new Vector2(vector.z, vector.z);
        public static Vector3 xxx(this Vector3 vector) => new Vector3(vector.x, vector.x, vector.x);
        public static Vector3 xxy(this Vector3 vector) => new Vector3(vector.x, vector.x, vector.y);
        public static Vector3 xxz(this Vector3 vector) => new Vector3(vector.x, vector.x, vector.z);
        public static Vector3 xyx(this Vector3 vector) => new Vector3(vector.x, vector.y, vector.x);
        public static Vector3 xyy(this Vector3 vector) => new Vector3(vector.x, vector.y, vector.y);
        public static Vector3 xyz(this Vector3 vector) => new Vector3(vector.x, vector.y, vector.z);
        public static Vector3 xzx(this Vector3 vector) => new Vector3(vector.x, vector.z, vector.x);
        public static Vector3 xzy(this Vector3 vector) => new Vector3(vector.x, vector.z, vector.y);
        public static Vector3 xzz(this Vector3 vector) => new Vector3(vector.x, vector.z, vector.z);
        public static Vector3 zxx(this Vector3 vector) => new Vector3(vector.z, vector.x, vector.x);
        public static Vector3 zxy(this Vector3 vector) => new Vector3(vector.z, vector.x, vector.y);
        public static Vector3 zxz(this Vector3 vector) => new Vector3(vector.z, vector.x, vector.z);
        public static Vector3 zyx(this Vector3 vector) => new Vector3(vector.z, vector.y, vector.x);
        public static Vector3 zyy(this Vector3 vector) => new Vector3(vector.z, vector.y, vector.y);
        public static Vector3 zyz(this Vector3 vector) => new Vector3(vector.z, vector.y, vector.z);
        public static Vector3 zzx(this Vector3 vector) => new Vector3(vector.z, vector.z, vector.x);
        public static Vector3 zzy(this Vector3 vector) => new Vector3(vector.z, vector.z, vector.y);
        public static Vector3 zzz(this Vector3 vector) => new Vector3(vector.z, vector.z, vector.z);
        public static Vector4 xxxx(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.x, vector.x);
        public static Vector4 xxxy(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.x, vector.y);
        public static Vector4 xxxz(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.x, vector.z);
        public static Vector4 xxyx(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.y, vector.x);
        public static Vector4 xxyy(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.y, vector.y);
        public static Vector4 xxyz(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.y, vector.z);
        public static Vector4 xxzx(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.z, vector.x);
        public static Vector4 xxzy(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.z, vector.y);
        public static Vector4 xxzz(this Vector3 vector) => new Vector4(vector.x, vector.x, vector.z, vector.z);
        public static Vector4 xyxx(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.x, vector.x);
        public static Vector4 xyxy(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.x, vector.y);
        public static Vector4 xyxz(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.x, vector.z);
        public static Vector4 xyyx(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.y, vector.x);
        public static Vector4 xyyy(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.y, vector.y);
        public static Vector4 xyyz(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.y, vector.z);
        public static Vector4 xyzx(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.x, vector.x);
        public static Vector4 xyzy(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.z, vector.y);
        public static Vector4 xyzz(this Vector3 vector) => new Vector4(vector.x, vector.y, vector.z, vector.z);
        public static Vector4 xzxx(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.x, vector.x);
        public static Vector4 xzxy(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.x, vector.y);
        public static Vector4 xzxz(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.x, vector.z);
        public static Vector4 xzyx(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.y, vector.x);
        public static Vector4 xzyy(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.y, vector.y);
        public static Vector4 xzyz(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.y, vector.z);
        public static Vector4 xzzx(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.z, vector.x);
        public static Vector4 xzzy(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.z, vector.y);
        public static Vector4 xzzz(this Vector3 vector) => new Vector4(vector.x, vector.z, vector.z, vector.z);
        public static Vector4 yxxx(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.x, vector.x);
        public static Vector4 yxxy(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.x, vector.y);
        public static Vector4 yxxz(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.x, vector.z);
        public static Vector4 yxyx(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.y, vector.x);
        public static Vector4 yxyy(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.y, vector.y);
        public static Vector4 yxyz(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.y, vector.z);
        public static Vector4 yxzx(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.z, vector.x);
        public static Vector4 yxzy(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.z, vector.y);
        public static Vector4 yxzz(this Vector3 vector) => new Vector4(vector.y, vector.x, vector.z, vector.z);
        public static Vector4 yyxx(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.x, vector.x);
        public static Vector4 yyxy(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.x, vector.y);
        public static Vector4 yyxz(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.x, vector.z);
        public static Vector4 yyyx(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.y, vector.x);
        public static Vector4 yyyy(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.y, vector.y);
        public static Vector4 yyyz(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.y, vector.z);
        public static Vector4 yyzx(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.x, vector.x);
        public static Vector4 yyzy(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.z, vector.y);
        public static Vector4 yyzz(this Vector3 vector) => new Vector4(vector.y, vector.y, vector.z, vector.z);
        public static Vector4 yzxx(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.x, vector.x);
        public static Vector4 yzxy(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.x, vector.y);
        public static Vector4 yzxz(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.x, vector.z);
        public static Vector4 yzyx(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.y, vector.x);
        public static Vector4 yzyy(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.y, vector.y);
        public static Vector4 yzyz(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.y, vector.z);
        public static Vector4 yzzx(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.z, vector.x);
        public static Vector4 yzzy(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.z, vector.y);
        public static Vector4 yzzz(this Vector3 vector) => new Vector4(vector.y, vector.z, vector.z, vector.z);
        public static Vector4 zxxx(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.x, vector.x);
        public static Vector4 zxxy(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.x, vector.y);
        public static Vector4 zxxz(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.x, vector.z);
        public static Vector4 zxyx(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.y, vector.x);
        public static Vector4 zxyy(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.y, vector.y);
        public static Vector4 zxyz(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.y, vector.z);
        public static Vector4 zxzx(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.z, vector.x);
        public static Vector4 zxzy(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.z, vector.y);
        public static Vector4 zxzz(this Vector3 vector) => new Vector4(vector.z, vector.x, vector.z, vector.z);
        public static Vector4 zyxx(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.x, vector.x);
        public static Vector4 zyxy(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.x, vector.y);
        public static Vector4 zyxz(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.x, vector.z);
        public static Vector4 zyyx(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.y, vector.x);
        public static Vector4 zyyy(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.y, vector.y);
        public static Vector4 zyyz(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.y, vector.z);
        public static Vector4 zyzx(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.x, vector.x);
        public static Vector4 zyzy(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.z, vector.y);
        public static Vector4 zyzz(this Vector3 vector) => new Vector4(vector.z, vector.y, vector.z, vector.z);
        public static Vector4 zzxx(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.x, vector.x);
        public static Vector4 zzxy(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.x, vector.y);
        public static Vector4 zzxz(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.x, vector.z);
        public static Vector4 zzyx(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.y, vector.x);
        public static Vector4 zzyy(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.y, vector.y);
        public static Vector4 zzyz(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.y, vector.z);
        public static Vector4 zzzx(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.z, vector.x);
        public static Vector4 zzzy(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.z, vector.y);
        public static Vector4 zzzz(this Vector3 vector) => new Vector4(vector.z, vector.z, vector.z, vector.z);
        
        #endregion Conversions
        
    }
}