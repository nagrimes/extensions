﻿// ReSharper disable InconsistentNaming
// ReSharper disable InvalidXmlDocComment
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CheckNamespace
// ReSharper disable UnusedMember.Global
// ReSharper disable IdentifierTypo

namespace RotaryHeart.Lib.Extensions
{
    public static class EnumExtensions
    {
        
        /// <summary>
        /// Returns a random value from an enum
        /// </summary>
        public static T GetRandomValue<T>() where T : System.Enum
        {
            System.Array values = System.Enum.GetValues(typeof(T));
            return (T)values.GetValue(UnityEngine.Random.Range(0, values.Length));
        }

        /// <summary>
        /// Returns all the values from an enum
        /// </summary>
        public static T[] GetEnumValues<T>() where T : System.Enum
        {
            return (T[])System.Enum.GetValues(typeof(T));
        }

        /// <summary>
        /// Function used to check if this enum mask contains a specific enum value
        /// </summary>
        /// <param name="checkValue">Value to check on mask</param>
        public static bool Contains<T>(this T thisType, T checkValue) where T : System.IConvertible
        {
            return thisType.Contains((int)(System.IConvertible)checkValue);
        }

        /// <summary>
        /// Function used to check if this enum mask contains a specific enum value
        /// </summary>
        /// <param name="checkValue">Value to check on mask</param>
        public static bool Contains<T>(this T thisType, int checkValue) where T : System.IConvertible
        {
            if (!typeof(T).IsEnum)
                throw new System.ArgumentException("Must be an enumerated type");

            return (((int)(System.IConvertible)thisType) & checkValue) != 0;
        }

    }
}