using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace RotaryHeart.Lib.Extensions
{
    /// <summary>
    /// Needs to be inside an editor folder
    /// </summary>
    public static class EditorExtensionMethods
    {
        //Used to draw rects with color
        static readonly Texture2D backgroundTexture = Texture2D.whiteTexture;
        static readonly GUIStyle textureStyle = new GUIStyle { normal = new GUIStyleState { background = backgroundTexture } };
        static PropertyInfo inspectorModeInfo = typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);

        /// <summary>
        /// Draws a rect with a solid color
        /// </summary>
        /// <param name="position">Position to draw the rect</param>
        /// <param name="color">Color to draw the rect</param>
        /// <param name="content">Content, if any</param>
        public static void DrawRect(Rect position, Color color, GUIContent content = null)
        {
            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
            GUI.Box(position, content ?? GUIContent.none, textureStyle);
            GUI.backgroundColor = backgroundColor;
        }

        /// <summary>
        /// Returns the FileId of an Object
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <returns>FileId value</returns>
        public static long GetFileId(Object obj)
        {
            if (obj == null)
                return -1;

            SerializedObject serializedObject = new SerializedObject(obj);
            inspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);

            SerializedProperty localIdProp = serializedObject.FindProperty("m_LocalIdentfierInFile");   //note the misspelling!

            return localIdProp.longValue;
        }
    }
}
